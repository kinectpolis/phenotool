﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;

namespace phenotypingTool.common {

    public abstract class Photo : IDisposable{
         public Image<Bgr, Byte> color{ get; set; }
         public Image<Gray, Byte> mask{ get; set; }
         //public VectorOfVectorOfPoint contourns {get; set;}
         public Dictionary<String, Bitmap> images;         
         public String path {set; get; }
         public abstract Task  extractBackground();
         public abstract Task  extractContourns();
         public abstract Task  process();
         public void Dispose(){
            if(color != null) color.Dispose();
            if(mask != null) mask.Dispose();
            images = null;
            
         }
    }
}