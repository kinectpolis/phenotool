﻿namespace phenotypingTool.beans.pollen
{
    partial class TrainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.fplImages = new System.Windows.Forms.FlowLayoutPanel();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dvTrainData = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnTrain = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.btnBrowseDataSet = new System.Windows.Forms.Button();
            this.txtDataSetFile = new System.Windows.Forms.TextBox();
            this.btnPlot = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnUnlabeled = new System.Windows.Forms.Button();
            this.btnUnselectAll = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.Classes = new System.Windows.Forms.GroupBox();
            this.rdbUnknown = new System.Windows.Forms.RadioButton();
            this.btnexport2excel = new System.Windows.Forms.Button();
            this.btnClassify = new System.Windows.Forms.Button();
            this.rdbIntermedium = new System.Windows.Forms.RadioButton();
            this.rdbNonViable = new System.Windows.Forms.RadioButton();
            this.rdbViable = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.radioButtonViablesAndNon = new System.Windows.Forms.RadioButton();
            this.radioButtonViablesAndInter = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvTrainData)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.Classes.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Size = new System.Drawing.Size(970, 497);
            this.splitContainer1.SplitterDistance = 685;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControl1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.groupBox1.Size = new System.Drawing.Size(685, 497);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Images";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(1, 16);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(683, 478);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.fplImages);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.tabPage1.Size = new System.Drawing.Size(675, 451);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Images";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // fplImages
            // 
            this.fplImages.AutoScroll = true;
            this.fplImages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fplImages.Location = new System.Drawing.Point(5, 3);
            this.fplImages.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.fplImages.Name = "fplImages";
            this.fplImages.Size = new System.Drawing.Size(665, 445);
            this.fplImages.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dvTrainData);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.tabPage2.Size = new System.Drawing.Size(447, 451);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Data";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dvTrainData
            // 
            this.dvTrainData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvTrainData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvTrainData.Location = new System.Drawing.Point(5, 3);
            this.dvTrainData.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.dvTrainData.Name = "dvTrainData";
            this.dvTrainData.Size = new System.Drawing.Size(437, 445);
            this.dvTrainData.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.Classes);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.groupBox2.Size = new System.Drawing.Size(280, 497);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tools";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radioButtonViablesAndInter);
            this.groupBox4.Controls.Add(this.radioButtonViablesAndNon);
            this.groupBox4.Controls.Add(this.btnTrain);
            this.groupBox4.Controls.Add(this.btnView);
            this.groupBox4.Controls.Add(this.btnPlot);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(1, 353);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupBox4.Size = new System.Drawing.Size(278, 133);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Train";
            // 
            // btnTrain
            // 
            this.btnTrain.Image = global::phenotypingTool.beans.pollen.Properties.Resources._004_robot;
            this.btnTrain.Location = new System.Drawing.Point(203, 63);
            this.btnTrain.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.btnTrain.Name = "btnTrain";
            this.btnTrain.Size = new System.Drawing.Size(67, 63);
            this.btnTrain.TabIndex = 4;
            this.btnTrain.UseVisualStyleBackColor = true;
            this.btnTrain.Click += new System.EventHandler(this.btnTrain_Click_1);
            // 
            // btnView
            // 
            this.btnView.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnView.Image = global::phenotypingTool.beans.pollen.Properties.Resources._002_binoculars_1;
            this.btnView.Location = new System.Drawing.Point(112, 62);
            this.btnView.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(83, 65);
            this.btnView.TabIndex = 3;
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // btnBrowseDataSet
            // 
            this.btnBrowseDataSet.Location = new System.Drawing.Point(229, 21);
            this.btnBrowseDataSet.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.btnBrowseDataSet.Name = "btnBrowseDataSet";
            this.btnBrowseDataSet.Size = new System.Drawing.Size(41, 21);
            this.btnBrowseDataSet.TabIndex = 2;
            this.btnBrowseDataSet.Text = "...";
            this.btnBrowseDataSet.UseVisualStyleBackColor = true;
            this.btnBrowseDataSet.Click += new System.EventHandler(this.btnBrowseDataSet_Click);
            // 
            // txtDataSetFile
            // 
            this.txtDataSetFile.Enabled = false;
            this.txtDataSetFile.Location = new System.Drawing.Point(60, 21);
            this.txtDataSetFile.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.txtDataSetFile.Name = "txtDataSetFile";
            this.txtDataSetFile.Size = new System.Drawing.Size(167, 20);
            this.txtDataSetFile.TabIndex = 1;
            // 
            // btnPlot
            // 
            this.btnPlot.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPlot.Image = global::phenotypingTool.beans.pollen.Properties.Resources._007_presentation;
            this.btnPlot.Location = new System.Drawing.Point(26, 63);
            this.btnPlot.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.btnPlot.Name = "btnPlot";
            this.btnPlot.Size = new System.Drawing.Size(77, 65);
            this.btnPlot.TabIndex = 0;
            this.btnPlot.UseVisualStyleBackColor = true;
            this.btnPlot.Click += new System.EventHandler(this.btnPlot_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnUnlabeled);
            this.groupBox3.Controls.Add(this.btnUnselectAll);
            this.groupBox3.Controls.Add(this.btnSelectAll);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(1, 238);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.groupBox3.Size = new System.Drawing.Size(278, 115);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Selection";
            // 
            // btnUnlabeled
            // 
            this.btnUnlabeled.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUnlabeled.Location = new System.Drawing.Point(7, 21);
            this.btnUnlabeled.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.btnUnlabeled.Name = "btnUnlabeled";
            this.btnUnlabeled.Size = new System.Drawing.Size(259, 21);
            this.btnUnlabeled.TabIndex = 3;
            this.btnUnlabeled.Text = "Unlabeled All";
            this.btnUnlabeled.UseVisualStyleBackColor = true;
            this.btnUnlabeled.Click += new System.EventHandler(this.btnUnlabeled_Click);
            // 
            // btnUnselectAll
            // 
            this.btnUnselectAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUnselectAll.Location = new System.Drawing.Point(7, 77);
            this.btnUnselectAll.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.btnUnselectAll.Name = "btnUnselectAll";
            this.btnUnselectAll.Size = new System.Drawing.Size(259, 21);
            this.btnUnselectAll.TabIndex = 2;
            this.btnUnselectAll.Text = "Unselect All";
            this.btnUnselectAll.UseVisualStyleBackColor = true;
            this.btnUnselectAll.Click += new System.EventHandler(this.btnUnselectAll_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelectAll.Location = new System.Drawing.Point(7, 49);
            this.btnSelectAll.Margin = new System.Windows.Forms.Padding(5, 3, 5, 3);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(259, 21);
            this.btnSelectAll.TabIndex = 1;
            this.btnSelectAll.Text = "Select All";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // Classes
            // 
            this.Classes.Controls.Add(this.label1);
            this.Classes.Controls.Add(this.rdbUnknown);
            this.Classes.Controls.Add(this.btnexport2excel);
            this.Classes.Controls.Add(this.btnBrowseDataSet);
            this.Classes.Controls.Add(this.btnClassify);
            this.Classes.Controls.Add(this.txtDataSetFile);
            this.Classes.Controls.Add(this.rdbIntermedium);
            this.Classes.Controls.Add(this.rdbNonViable);
            this.Classes.Controls.Add(this.rdbViable);
            this.Classes.Dock = System.Windows.Forms.DockStyle.Top;
            this.Classes.Location = new System.Drawing.Point(1, 16);
            this.Classes.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.Classes.Name = "Classes";
            this.Classes.Padding = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.Classes.Size = new System.Drawing.Size(278, 222);
            this.Classes.TabIndex = 0;
            this.Classes.TabStop = false;
            this.Classes.Text = "Classification";
            // 
            // rdbUnknown
            // 
            this.rdbUnknown.AutoSize = true;
            this.rdbUnknown.Location = new System.Drawing.Point(89, 113);
            this.rdbUnknown.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.rdbUnknown.Name = "rdbUnknown";
            this.rdbUnknown.Size = new System.Drawing.Size(70, 18);
            this.rdbUnknown.TabIndex = 5;
            this.rdbUnknown.TabStop = true;
            this.rdbUnknown.Text = "unknown";
            this.rdbUnknown.UseVisualStyleBackColor = true;
            // 
            // btnexport2excel
            // 
            this.btnexport2excel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnexport2excel.Image = global::phenotypingTool.beans.pollen.Properties.Resources._011_save;
            this.btnexport2excel.Location = new System.Drawing.Point(138, 154);
            this.btnexport2excel.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnexport2excel.Name = "btnexport2excel";
            this.btnexport2excel.Size = new System.Drawing.Size(59, 43);
            this.btnexport2excel.TabIndex = 4;
            this.btnexport2excel.UseVisualStyleBackColor = true;
            this.btnexport2excel.Click += new System.EventHandler(this.btnexport2excel_Click);
            // 
            // btnClassify
            // 
            this.btnClassify.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClassify.Image = global::phenotypingTool.beans.pollen.Properties.Resources._001_checked;
            this.btnClassify.Location = new System.Drawing.Point(73, 154);
            this.btnClassify.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.btnClassify.Name = "btnClassify";
            this.btnClassify.Size = new System.Drawing.Size(55, 43);
            this.btnClassify.TabIndex = 3;
            this.btnClassify.UseVisualStyleBackColor = true;
            this.btnClassify.Click += new System.EventHandler(this.btnClassify_Click);
            // 
            // rdbIntermedium
            // 
            this.rdbIntermedium.AutoSize = true;
            this.rdbIntermedium.Location = new System.Drawing.Point(89, 91);
            this.rdbIntermedium.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.rdbIntermedium.Name = "rdbIntermedium";
            this.rdbIntermedium.Size = new System.Drawing.Size(82, 18);
            this.rdbIntermedium.TabIndex = 2;
            this.rdbIntermedium.TabStop = true;
            this.rdbIntermedium.Text = "Intermedium";
            this.rdbIntermedium.UseVisualStyleBackColor = true;
            // 
            // rdbNonViable
            // 
            this.rdbNonViable.AutoSize = true;
            this.rdbNonViable.Location = new System.Drawing.Point(89, 71);
            this.rdbNonViable.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.rdbNonViable.Name = "rdbNonViable";
            this.rdbNonViable.Size = new System.Drawing.Size(78, 18);
            this.rdbNonViable.TabIndex = 1;
            this.rdbNonViable.TabStop = true;
            this.rdbNonViable.Text = "Non-Viable";
            this.rdbNonViable.UseVisualStyleBackColor = true;
            // 
            // rdbViable
            // 
            this.rdbViable.AutoSize = true;
            this.rdbViable.Location = new System.Drawing.Point(89, 50);
            this.rdbViable.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.rdbViable.Name = "rdbViable";
            this.rdbViable.Size = new System.Drawing.Size(55, 18);
            this.rdbViable.TabIndex = 0;
            this.rdbViable.TabStop = true;
            this.rdbViable.Text = "Viable";
            this.rdbViable.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 21);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 14);
            this.label1.TabIndex = 6;
            this.label1.Text = "Dataset";
            // 
            // radioButtonViablesAndNon
            // 
            this.radioButtonViablesAndNon.AutoSize = true;
            this.radioButtonViablesAndNon.Location = new System.Drawing.Point(84, 14);
            this.radioButtonViablesAndNon.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.radioButtonViablesAndNon.Name = "radioButtonViablesAndNon";
            this.radioButtonViablesAndNon.Size = new System.Drawing.Size(141, 18);
            this.radioButtonViablesAndNon.TabIndex = 7;
            this.radioButtonViablesAndNon.TabStop = true;
            this.radioButtonViablesAndNon.Text = "Viables and non-viables";
            this.radioButtonViablesAndNon.UseVisualStyleBackColor = true;
            // 
            // radioButtonViablesAndInter
            // 
            this.radioButtonViablesAndInter.AutoSize = true;
            this.radioButtonViablesAndInter.Location = new System.Drawing.Point(84, 36);
            this.radioButtonViablesAndInter.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.radioButtonViablesAndInter.Name = "radioButtonViablesAndInter";
            this.radioButtonViablesAndInter.Size = new System.Drawing.Size(147, 18);
            this.radioButtonViablesAndInter.TabIndex = 7;
            this.radioButtonViablesAndInter.TabStop = true;
            this.radioButtonViablesAndInter.Text = "viables and intermediates";
            this.radioButtonViablesAndInter.UseVisualStyleBackColor = true;
            // 
            // TrainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(970, 497);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(1, 3, 1, 3);
            this.Name = "TrainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TrainForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dvTrainData)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.Classes.ResumeLayout(false);
            this.Classes.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.FlowLayoutPanel fplImages;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox Classes;
        private System.Windows.Forms.Button btnClassify;
        private System.Windows.Forms.RadioButton rdbIntermedium;
        private System.Windows.Forms.RadioButton rdbNonViable;
        private System.Windows.Forms.RadioButton rdbViable;
        private System.Windows.Forms.Button btnexport2excel;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnUnselectAll;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnUnlabeled;
        private System.Windows.Forms.RadioButton rdbUnknown;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnBrowseDataSet;
        private System.Windows.Forms.TextBox txtDataSetFile;
        private System.Windows.Forms.Button btnPlot;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dvTrainData;
        private System.Windows.Forms.Button btnTrain;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButtonViablesAndInter;
        private System.Windows.Forms.RadioButton radioButtonViablesAndNon;
    }
}