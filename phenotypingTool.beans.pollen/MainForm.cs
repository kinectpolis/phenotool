﻿using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using MathNet.Numerics.Statistics;
using phenotypingTool.common;
using phenotypingTool.cv;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace phenotypingTool.beans.pollen {
    public partial class MainForm : Form {

        private readonly SynchronizationContext synchronizationContext;
        private Dictionary<String, PollenGrainsPhoto> photos;
        private SVM ForViablesAndNon;
        private SVM ForViablesAndInter;

        public MainForm() {
            InitializeComponent();
            this.synchronizationContext = SynchronizationContext.Current;
            this.photos = new Dictionary<string, PollenGrainsPhoto>();
            string pathModel = Path.Combine(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "pollenModel.xml");
            this.ForViablesAndNon = new SVM();
            using (FileStorage f = new FileStorage(pathModel, FileStorage.Mode.Read)) {
                this.ForViablesAndNon.Read(f.GetRoot());                    
            }

            pathModel = Path.Combine(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "pollenModelViablesAndInterm.xml");
            this.ForViablesAndInter = new SVM();
            using (FileStorage f = new FileStorage(pathModel, FileStorage.Mode.Read)) {
                this.ForViablesAndInter.Read(f.GetRoot());                    
            }
            
        }


        private PictureBox createPictureBox(Bitmap img, Object tag = null)
        {
            #region make Picture Box
            PictureBox pbx = new PictureBox();
            pbx.Image = img;
            pbx.Size = new Size(150, 150);
            pbx.BorderStyle = BorderStyle.FixedSingle;
            pbx.Margin = new Padding(10);
            pbx.Cursor = Cursors.Hand;
            pbx.Tag = tag;
            pbx.SizeMode = PictureBoxSizeMode.Zoom;
            pbx.Dock = DockStyle.Fill;
            pbx.ContextMenuStrip = this.contextMenuImage;
            pbx.DoubleClick += displayImage_PictureBox_Click;
            //img.Dispose();
            return pbx; 
            #endregion
        }

        private void displayImage_PictureBox_Click(object sender, EventArgs e) {
             PictureBox pbox = sender as PictureBox;
            Image<Bgr, Byte> imageClicked = new Image<Bgr, Byte>(pbox.Image as Bitmap);
            Emgu.CV.UI.ImageViewer viewer = new Emgu.CV.UI.ImageViewer(imageClicked);
            viewer.ShowDialog();
          
        }


        private async void btnBrowseFolder_Click(object sender, EventArgs e) {
            try {


                FolderBrowserDialog dialog = new FolderBrowserDialog();
                if (dialog.ShowDialog() == DialogResult.OK) {
                    var files = Directory.EnumerateFiles(dialog.SelectedPath);
                    txtColorImagesPath.Text = dialog.SelectedPath;
                    files = files.Where(f => Utilities.ImageExtensions.Contains(Path.GetExtension(f.ToUpper())));
                    if (files.Count() > 0) {
                        this.photos = new Dictionary<string, PollenGrainsPhoto>();
                        this.flpImages.Controls.Clear();
                        await this.readImages(files.ToArray());
                        MessageBox.Show("Images Loaded", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else {
                        MessageBox.Show("Images noWt found", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
        }
            catch (Exception ex) {
                MessageBox.Show(ex.Message,"", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
}

        private Task readImages(string[] files){
            #region read Images
            return Task.WhenAll(
               files.Select(imagePath =>
                   Task.Run(async delegate (){
                       try{
                       PollenGrainsPhoto img = new PollenGrainsPhoto(imagePath);
                       await img.process();                      
                       foreach(PollenGrain pollen in img.pollenGrains) {
                          Dictionary<string, string> vfeatures =  await pollen.getVectorFeatures();
                          float[] features = vfeatures.Where( f => f.Key != "id" && f.Key != "class" ).Select( f => Convert.ToSingle(f.Value)).ToArray();
                          Matrix<float> row = FeaturesUtilities.vector2matrix(features);
                           float viability = this.ForViablesAndNon.Predict(row);
                           //Console.WriteLine(Enum.GetName(typeof(PollenType), Convert.ToInt32(viability)));;
                           //PollenType predictedLabel = (PollenType)Enum.ToObject(typeof(PollenType) , Convert.ToInt32(viability));
                           switch(Convert.ToInt32(viability)){
                               case 1: 
                                    //img.color.Draw(String.Format("{0:0.##}", viability), pollen.blob.Rectangle.Location, Emgu.CV.CvEnum.FontFace.HersheyPlain,1,new Bgr(0,255,0),2);
                                    if(!checkBoxIntermediates.Checked){
                                        img.color.Draw(new Cross2DF(new PointF(pollen.blob.CenterOfGravity.X,pollen.blob.CenterOfGravity.Y) ,10, 10), new Bgr(0,255,0),2);
                                        pollen.type =  PollenType.viable;
                                    } else {
                                       float[] v = await pollen.getVectorFeatures2();
                                       row = FeaturesUtilities.vector2matrix(v);
                                       viability = this.ForViablesAndInter.Predict(row);
                                       System.Diagnostics.Debug.WriteLine(viability);
                                           if(viability == 1) {
                                               img.color.Draw(new Cross2DF(new PointF(pollen.blob.CenterOfGravity.X, pollen.blob.CenterOfGravity.Y), 10, 10), new Bgr(0, 255, 0), 2);
                                               pollen.type = PollenType.viable;

                                           } else if(viability == 3) {
                                               img.color.Draw(new Cross2DF(new PointF(pollen.blob.CenterOfGravity.X, pollen.blob.CenterOfGravity.Y), 10, 10), new Bgr(255, 0, 0), 2);
                                               pollen.type = PollenType.intermedium;
                                           }
                                       }
                                break;
                                case 2: 
                                    //img.color.Draw(String.Format("{0:0.##}", viability), pollen.blob.Rectangle.Location, Emgu.CV.CvEnum.FontFace.HersheyPlain,1,new Bgr(255,0,255),2);
                                    img.color.Draw(new Cross2DF(new PointF(pollen.blob.CenterOfGravity.X,pollen.blob.CenterOfGravity.Y) ,10, 10), new Bgr(255,0,255),2); 
                                    pollen.type =  PollenType.nonViable;
                                break;
                           }
                       }  
                       
                        img.images.Add("color", img.color.ToBitmap());
                        img.images.Add("mask",  img.mask.ToBitmap());
                        img.color.Dispose();
                        img.color.Dispose();
                       //float viability = this.predictionModel.Predict(row);
                       this.photos.Add(Path.GetFileNameWithoutExtension(imagePath), img);
                       synchronizationContext.Send(new SendOrPostCallback(o =>{
                           this.showImage(o as PollenGrainsPhoto);
                       }), img);
                       }
                       catch(Exception ex) {
                           MessageBox.Show(ex.Message);
                       }
                   }
               )));  
            #endregion
        }

        
        private void showImage(Photo pic){
            #region createContainer
            Panel pnlLayout = new Panel();
            pnlLayout.Size = new Size(180, 220);
            pnlLayout.BorderStyle = BorderStyle.FixedSingle;

            //tab control
            TabControl tabControl = new TabControl();
            tabControl.Size = new Size(180, 180);
            tabControl.Dock = DockStyle.Top;
            //tabControl.Font = new Font("Arial", 7);
            tabControl.Appearance = TabAppearance.Normal;
            tabControl.Cursor = Cursors.Hand;

            foreach (var entry in pic.images){
                //color tab page
                String fileName = Path.GetFileNameWithoutExtension(pic.path);
                PictureBox pictureBox = this.createPictureBox(entry.Value, fileName);
                TabPage tabPage = new TabPage();
                tabPage.Text = entry.Key;   
                tabPage.Controls.Add(pictureBox);
                tabControl.TabPages.Add(tabPage);
            }

            Panel pnlInfo = new Panel();
            pnlInfo.Size = new Size(180, 60);
            pnlInfo.Dock = DockStyle.Bottom;
            Label lblFileName = new Label();
            //lblFileName.BorderStyle = BorderStyle.FixedSingle;
            lblFileName.Text = Path.GetFileName(pic.path);
            lblFileName.Dock = DockStyle.Fill;
            pnlInfo.Controls.Add(lblFileName);
            pnlLayout.Controls.Add(pnlInfo);
            pnlLayout.Controls.Add(tabControl);
            this.flpImages.Controls.Add(pnlLayout);
            #endregion
        }

        private void trainToolStripMenuItem_Click(object sender, EventArgs e){
            // Try to cast the sender to a ToolStripItem
            ToolStripItem menuItem = sender as ToolStripItem;
            if (menuItem != null){
                // Retrieve the ContextMenuStrip that owns this ToolStripItem
                ContextMenuStrip owner = menuItem.Owner as ContextMenuStrip;
                if (owner != null) {
                    // Get the control that is displaying this context menu
                    PictureBox sourceControl = (PictureBox)owner.SourceControl;
                    String fileName = sourceControl.Tag as String;
                    PollenGrainsPhoto src= this.photos[fileName];
                    TrainForm form = new TrainForm(src.pollenGrains);
                    form.ShowDialog();
                }
            }
           
        }

        private void shapeHistogramToolStripMenuItem_Click(object sender, EventArgs e) {
            // Try to cast the sender to a ToolStripItem
            try {
                ToolStripItem menuItem = sender as ToolStripItem;
                if(menuItem != null) {
                    // Retrieve the ContextMenuStrip that owns this ToolStripItem
                    ContextMenuStrip owner = menuItem.Owner as ContextMenuStrip;
                    if(owner != null) {
                        // Get the control that is displaying this context menu
                        PictureBox sourceControl = (PictureBox)owner.SourceControl;
                        String fileName = sourceControl.Tag as String;
                        PollenGrainsPhoto src = this.photos[fileName];
                        double[] data = src.pollenGrains.Select(grain => grain.shape).ToArray();
                        double median = data.Median();                                 
                        double upper = data.Percentile(95);//data.UpperQuartile();
                        double lower = data.Percentile(5);//data.LowerQuartile();
                        Console.WriteLine(String.Format("{0},{1},{2}", lower, median, upper));
                        //Accord.Controls.HistogramBox.Show(data, "shape");

                    }
                }
            } catch(Exception ex) {
                MessageBox.Show(ex.Message,"", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void areaHistogramToolStripMenuItem_Click(object sender, EventArgs e) {
            try{
             ToolStripItem menuItem = sender as ToolStripItem;
            if (menuItem != null){
                // Retrieve the ContextMenuStrip that owns this ToolStripItem
                ContextMenuStrip owner = menuItem.Owner as ContextMenuStrip;
                if (owner != null) {
                    // Get the control that is displaying this context menu
                    PictureBox sourceControl = (PictureBox)owner.SourceControl;
                    String fileName = sourceControl.Tag as String;
                    PollenGrainsPhoto src= this.photos[fileName];
                    double[] data = src.pollenGrains.Select(grain => Convert.ToDouble(grain.blob.Area) ).ToArray();
                    double median = data.Median();             
                    double upper = data.Percentile(95);//data.UpperQuartile();
                    double lower = data.Percentile(5);//data.LowerQuartile();
                    Console.WriteLine(String.Format("{0},{1},{2}", lower, median, upper));
                    //Accord.Controls.HistogramBox.Show(data, "Area");
                    //Accord.Controls.ScatterplotBox.Show(data);                   
                    
                }
            }
           }
            catch(Exception ex) {
              MessageBox.Show(ex.Message,"", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnExportToCsv_Click(object sender, EventArgs e) {
              try{
                  SaveFileDialog dialog = new SaveFileDialog();
                  dialog.Filter = "CSV Files (*.csv)|*.csv";
                 dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                if(dialog.ShowDialog() == DialogResult.OK) 
                     using(var csv = new StreamWriter(dialog.FileName, false)) {
                        csv.WriteLine("FileName, viables, nonViables, Intermedios");
                          foreach(var image in this.photos){                             
                                int viablesCount =  image.Value.pollenGrains.Where( grain  => grain.type == PollenType.viable).Count();
                                int nonViablesCount = image.Value.pollenGrains.Where( grain  => grain.type == PollenType.nonViable).Count();
                                int  intermediusCount =  image.Value.pollenGrains.Where( grain  => grain.type == PollenType.intermedium).Count();
                                csv.WriteLine(String.Format("{0},{1},{2},{3}", image.Key, viablesCount, nonViablesCount, intermediusCount));
                                csv.Flush();
                           }
                          MessageBox.Show("file exported succesfully","", MessageBoxButtons.OK, MessageBoxIcon.Information);
                      }
             }
            catch(Exception ex) {
              MessageBox.Show(ex.Message,"", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
