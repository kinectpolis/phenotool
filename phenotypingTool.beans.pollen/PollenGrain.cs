﻿using Accord.Imaging;
using Emgu.CV;
using Emgu.CV.Structure;
using phenotypingTool.cv;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace phenotypingTool.beans.pollen {
    public enum PollenType { viable = 1, nonViable = 2, intermedium = 3, unknown = 4 }
    public class PollenGrain{
        public int width{ get; set; }
        public int height { get; set; }
        public float centroideY;
        public double area{ get; set; } 
        public Guid id {get; set; }
        public double eccentricity{get;set;}
        public int npoints{get;set;}
        public Blob blob{ get; set; }
        public double shape{ get; set;}
        public double perimeter{ get; set; }
        public PollenType type{ get;set; }
        public float centroideX { get; internal set; }
        public float radius { get; internal set; }
        public float aspectRatio { get; internal set; }
        public double extent { get; internal set; }
        public MCvScalar hsvAvg { get; internal set; }
        public MCvScalar bgrAvg { get; internal set; }
        public MCvScalar labAvg { get; internal set; }
        public double homogeneity { get; internal set; }
        public double contrast { get; internal set; }
        public double[] haralickFeatures { get; internal set; }

        public PollenGrain(Blob blob) {
            this.blob = blob;
            this.eccentricity = 0;
            this.perimeter = 0;
            this.shape = 0;
            this.type = PollenType.unknown;
            this.npoints=0;
            this.area = 0;
            this.id = Guid.NewGuid();
        }

     

        internal Task<Dictionary<string, string>> getVectorFeatures() {
            Dictionary<String, String> features = new Dictionary<string, string>();
            return Task.Run(delegate(){
                features.Add("id", this.id.ToString());
                features.Add("width", this.width.ToString());
                features.Add("height", this.height.ToString());
                features.Add("homogeneity", String.Format("{0:0.##}",this.homogeneity));
                features.Add("contrast",String.Format("{0:0.##}",this.contrast));
                features.Add("perimeter",String.Format("{0:0.##}",this.perimeter));
                features.Add("shape",String.Format("{0:0.##}",this.shape));
                features.Add("aspectRatio",String.Format("{0:0.##}",this.aspectRatio));
                features.Add("area",String.Format("{0:0.##}",this.area));
                features.Add("centroideX",String.Format("{0:0.##}",this.centroideX));
                features.Add("centroideY",String.Format("{0:0.##}",this.centroideY));
                features.Add("extent",String.Format("{0:0.##}",this.extent));
                features.Add("radius",String.Format("{0:0.##}",this.radius));
                features.Add("avgBgrB", this.bgrAvg.V0.ToString());
                features.Add("avgBgrG", this.bgrAvg.V1.ToString());
                features.Add("avgBgrR", this.bgrAvg.V2.ToString());
                features.Add("avgHsvH", this.hsvAvg.V0.ToString());
                features.Add("avgHsvS", this.hsvAvg.V1.ToString());
                features.Add("avgHsvV", this.hsvAvg.V2.ToString());
                features.Add("npoints", this.npoints.ToString());
                //for(int i = 0; i < this.haralickFeatures.Length; i++) {
                //    features.Add(String.Format("f{0}", i), String.Format("{0:0.##}",this.haralickFeatures[i]));
                //}
                features.Add("class", String.Format("{0}", (int)this.type));

                return features;
            });
        }

        internal Task<float[]> getVectorFeatures2() {
           return Task.Run(delegate(){
              using(Image<Bgr, Byte> src = new Image<Bgr, byte>(this.blob.Image.ToManagedImage())){
                    float[] V1 = FeaturesUtilities.image_characteristics_to_feature(src);
                    float[] V2 = FeaturesUtilities.segments_to_vector_feature(src); 
                    float[] vector_of_features= new float[V1.Length + V2.Length];
                    Array.Copy(V1, vector_of_features, V1.Length);
                    Array.Copy(V2, 0, vector_of_features, V1.Length, V2.Length);
                   return vector_of_features;
               }
           });
        }



    }
}