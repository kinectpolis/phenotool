﻿using Accord.Controls;
using Accord.IO;
using Accord.Math;
using Accord.Statistics.Analysis;
using Accord.Statistics.Models.Regression.Linear;
using Accord.Statistics;
using Emgu.CV;
using Emgu.CV.ML;
using Emgu.CV.Structure;
using phenotypingTool.cv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Statistics.Kernels;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.Performance;
using Accord.Math.Optimization.Losses;
using Accord.MachineLearning;

namespace phenotypingTool.beans.pollen
{
    public enum ValidationType{
        RMS = 1, //Root mean squared error
        MSE = 2, // Mean squared normalized error
        HITS = 3,
        MAE= 4, //Mean Absolute Error (MAE)
        R2 = 5    //Coefficient of Determination (R2)
    }

    public partial class TrainForm : Form
    {
        private List<PollenGrain> pollenGrains;
        private const string PICTURE_BOX_NAME = "pictureBoxPollenGrainImage";
        CsvReader csvReader;
        
        public TrainForm(List<PollenGrain> pollenGrains)
        {
            this.pollenGrains = pollenGrains;
            InitializeComponent();
            this.LoadImages();
        }

        
        private void LoadImages()
        {
            foreach (PollenGrain grain in this.pollenGrains)
            {
                if(grain.blob != null) {

                    Bitmap img = grain.blob.Image.ToManagedImage();
                    Panel pnlLayout = new Panel();
                    pnlLayout.Size = new Size(50, 100);
                    pnlLayout.BorderStyle = BorderStyle.FixedSingle;
                    Color  color = this.GetColor(grain.type);
                    pnlLayout.BackColor = color;
                    CheckBox cbx = new CheckBox();
                    cbx.Dock = DockStyle.Top;
                    //color tab page
                    //Emgu.CV.UI.ImageBox pbx = new  Emgu.CV.UI.ImageBox();
                    PictureBox pbx = new PictureBox();
                    pbx.Name = PICTURE_BOX_NAME;
                    pbx.Image = img;
                    pbx.Size = new Size(150, 150);
                    pbx.BorderStyle = BorderStyle.FixedSingle;
                    pbx.Margin = new Padding(10);
                    pbx.Cursor = Cursors.Hand;
                    pbx.Tag = grain;
                    pbx.SizeMode = PictureBoxSizeMode.Zoom;
                    pbx.Dock = DockStyle.Fill;
                    pbx.Controls.Add(cbx);
                    pnlLayout.Controls.Add(pbx);
                    this.fplImages.Controls.Add(pnlLayout);
                    
                }

            }
        }
        private Color GetColor(PollenType label)
        {
            switch (label)
            {
                case PollenType.viable:
                    return Color.Lime;
                case PollenType.nonViable:
                    return Color.DarkGreen;
                case PollenType.intermedium:
                    return Color.LightGreen;
                default:
                    return this.BackColor;
            }
        }
        private void btnClassify_Click(object sender, EventArgs e)
        {
            PollenType label = PollenType.unknown;
            if (this.rdbViable.Checked) {label = PollenType.viable; }
            else if (this.rdbNonViable.Checked) {  label = PollenType.nonViable; }
            else if (this.rdbIntermedium.Checked) { label = PollenType.intermedium; }
            
            Color color = this.GetColor(label);
            foreach(Control control in this.fplImages.Controls){
                Panel container = control as Panel;
                Control[] matches= container.Controls.Find(PICTURE_BOX_NAME, true);
                    if(matches.Length > 0 && matches.First() is PictureBox){
                        PictureBox pictureBox = matches.First() as PictureBox;
                        CheckBox checkBox = pictureBox.Controls[0] as CheckBox;
                        if (checkBox.CheckState == CheckState.Checked){
                            pictureBox.BackColor = color;
                            checkBox.Checked = false;
                            PollenGrain image = (PollenGrain) pictureBox.Tag;
                            image.type = label;

                        }
                    }
            }

            MessageBox.Show("Classification done", "", MessageBoxButtons.OK, MessageBoxIcon.Information) ;

        }

        private async void btnexport2excel_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(this.txtDataSetFile.Text)){ 
                try {
                    string pathCsv = txtDataSetFile.Text;//Path.Combine(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "dataset.csv");
                    string pathImages = Path.Combine(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "samples");
                    using(var csv = new StreamWriter(pathCsv, true)) {
                        //if (MessageBox.Show("do you like update the model?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No){
                        foreach(Control control in this.fplImages.Controls) {
                            Panel container = control as Panel;
                            Control[] matches = container.Controls.Find(PICTURE_BOX_NAME, true);
                            if(matches.Length > 0 && matches.First() is PictureBox) {
                                PictureBox pictureBox = matches.First() as PictureBox;
                                PollenGrain image = (PollenGrain)pictureBox.Tag;
                                if(image.type != PollenType.unknown) {
                                    DirectoryInfo classDirectory = new DirectoryInfo(Path.Combine(pathImages, Enum.GetName(typeof(PollenType), (int)image.type)));
                                    if(!classDirectory.Exists)
                                        classDirectory.Create();
                                    image.blob.Image.ToManagedImage().Save(Path.Combine(classDirectory.FullName, String.Format("{0}.jpg", image.id.ToString())));
                                    Dictionary<String, String> features = await image.getVectorFeatures();
                                    csv.WriteLine(String.Join(",", features.Values));
                                    csv.Flush();
                                }

                            }
                        }
                    }
                    MessageBox.Show("dataset already updated", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else {
                MessageBox.Show("You must select the data file","", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btnSelectAll_Click(object sender, EventArgs e) {
            foreach(Control control in this.fplImages.Controls){
                Panel container = control as Panel;
                Control[] matches= container.Controls.Find(PICTURE_BOX_NAME, true);
                    if(matches.Length > 0 && matches.First() is PictureBox){
                        PictureBox pictureBox = matches.First() as PictureBox;
                        CheckBox checkBox = pictureBox.Controls[0] as CheckBox;
                        if(pictureBox.Tag != null && pictureBox.Tag is PollenGrain){
                            PollenGrain image = (PollenGrain) pictureBox.Tag;
                            checkBox.Checked = true;   
                        }
                    }
            }
        }

        private void btnUnselectAll_Click(object sender, EventArgs e) {
           foreach(Control control in this.fplImages.Controls){
                Panel container = control as Panel;
                Control[] matches= container.Controls.Find(PICTURE_BOX_NAME, true);
                    if(matches.Length > 0 && matches.First() is PictureBox){
                        PictureBox pictureBox = matches.First() as PictureBox;
                        CheckBox checkBox = pictureBox.Controls[0] as CheckBox;
                        if(pictureBox.Tag != null && pictureBox.Tag is PollenGrain){
                            PollenGrain image = (PollenGrain) pictureBox.Tag; 
                            checkBox.Checked = false;   
                        }
                    }
            }
        }

        private void btnUnlabeled_Click(object sender, EventArgs e) {
            Color color = this.GetColor(PollenType.unknown);
              foreach(Control control in this.fplImages.Controls){
                Panel container = control as Panel;
                Control[] matches= container.Controls.Find(PICTURE_BOX_NAME, true);
                    if(matches.Length > 0 && matches.First() is PictureBox){
                        PictureBox pictureBox = matches.First() as PictureBox;
                        CheckBox checkBox = pictureBox.Controls[0] as CheckBox;
                        if(pictureBox.Tag != null && pictureBox.Tag is PollenGrain){
                            pictureBox.BackColor = color;
                            checkBox.Checked = false;
                            PollenGrain image = (PollenGrain) pictureBox.Tag;
                            image.type = PollenType.unknown;   
                        }
                    }
            }
        }

        private void btnBrowseDataSet_Click(object sender, EventArgs e) {
            try {
                OpenFileDialog fd = new OpenFileDialog();
                fd.Filter = "CSV Files (*.csv)|*.csv";
                fd.InitialDirectory = System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath);
                if(fd.ShowDialog() == DialogResult.OK) {
                    txtDataSetFile.Text = fd.FileName;                     
                }
            } catch(Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnView_Click(object sender, EventArgs e) {
            if(!String.IsNullOrEmpty(this.txtDataSetFile.Text)){
                this.csvReader = new CsvReader(txtDataSetFile.Text, hasHeaders: false);
                DataTable dataSet = this.csvReader.ToTable();
                this.dvTrainData.DataSource = dataSet;
                this.dvTrainData.Update();
            } else {
                MessageBox.Show("You must select the data file","", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

         static double evaluate(Matrix<float> original, Matrix<float> predicted, ValidationType type) {

            int n = original.Rows;
            double diff = 0;
            double v = 0;
            int hits = 0;
            switch (type) {
                case ValidationType.HITS:
                    for (int i = 0; i < n; i++) {
                        if (original[i, 0] == predicted[i, 0]) {
                            hits++;
                        }
                    }
                    return (hits * 100) / n;
                case ValidationType.MSE:
                    diff = 0;
                    for (int i = 0; i < n; i++) {
                        //Console.WriteLine("[{0} => {1}] \n", original[i, 0], predicted[i, 0]);
                        diff += (original[i, 0] - predicted[i, 0]);
                    }
                    return diff / n;
                case ValidationType.RMS:
                    diff = 0;
                        for (int i = 0; i < n; i++) {
                            diff += (original[i, 0] - predicted[i, 0]);
                        }
                        return Math.Sqrt(Math.Pow(diff,2) / n);
                case ValidationType.MAE:
                    diff = 0;
                    for (int i = 0; i < n; i++) {
                        diff += Math.Abs(original[i, 0] - predicted[i, 0]);
                    }
                    return diff;
                case ValidationType.R2:
                    diff = 0;
                    v = 0;
                    MCvScalar mean = CvInvoke.Mean(original);
                    for (int i = 0; i < n; i++) {
                        diff += original[i, 0] - predicted[i, 0];
                        v += predicted[i, 0] - mean.V0;
                    }
                    return 1 - (Math.Pow(diff,2) / Math.Pow(v,2) );
                default:
                    return -1;
            }
        }

        private  void btnPlot_Click(object sender, EventArgs e) {
             if(!String.IsNullOrEmpty(this.txtDataSetFile.Text)){ 
            // Read the Excel worksheet into a DataTable
            CsvReader csvReader = new CsvReader(txtDataSetFile.Text, hasHeaders: false);
            // Convert the DataTable to input and output vectors
            DataTable dataset = csvReader.ToTable();
            String[] xIdxs = dataset.Columns.Cast<DataColumn>().Skip(1).Take(dataset.Columns.Count - 2).Select(col => col.ColumnName).ToArray();
            String yIdxs = dataset.Columns.Cast<DataColumn>().Select(col => col.ColumnName).ElementAt(dataset.Columns.Count - 1);
            DataTable xAsDataTable= new DataView(dataset).ToTable(false, xIdxs);
            DataTable yAsDataTable= new DataView(dataset).ToTable(false, yIdxs);
            double[][] X = xAsDataTable.Rows.Cast<DataRow>()
                .Select(
                    row => xAsDataTable
                        .Columns
                            .Cast<DataColumn>()
                                .Select(
                                col => Convert.ToDouble( row[col].ToString() ))
                                    .ToArray()
                               
                ).ToArray();
            
            int[] Y = yAsDataTable.Rows.Cast<DataRow>().Select(row => Convert.ToInt32( row[0].ToString())).ToArray();
            
                          
            double[][] C = X.Covariance();
            var pca = new PrincipalComponentAnalysis(){ Method = PrincipalComponentMethod.CovarianceMatrix };             
            MultivariateLinearRegression transform = pca.Learn(C);// Now we can learn the linear projection from the data
            pca.NumberOfOutputs = 2;//NumberOfOutputs to the desired components            
            double[][] components = pca.Transform(X);// Finally, we can project all the data 
             ScatterplotBox.Show("Pollen Viability DataSet", components, Y);//just plotting the dataset data 
            } 
             else {
                MessageBox.Show("You must select the data file","", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           
        }

        private void btnTrain_Click_1(object sender, EventArgs e) {
             if(!String.IsNullOrEmpty(this.txtDataSetFile.Text)){   
                  int y = 0;
                    Mat samples = new Mat();
                    using (var rd = new StreamReader(txtDataSetFile.Text)) {
                        while (!rd.EndOfStream) {
                            var line = rd.ReadLine().Split(',').Skip(1).ToArray();
                            Matrix<float> row = new Matrix<float>(new Size(line.Count(), 1));
                            for (int x = 0; x < line.Count(); x++) {
                                row[0, x] = Convert.ToSingle(line[x]);
                            }
                            samples.PushBack(row.Mat);
                            row.Dispose();
                            y++;
                        }
                    } 
                   Mat matX = new Mat(samples, new Rectangle(0, 0, samples.Width - 1, samples.Rows));
                   Mat matY = new Mat(samples, new Rectangle(samples.Width - 1, 0,1, samples.Rows));
                   Matrix<float> X = new Matrix<float>(new Size(matX.Width, matX.Height));
                   Matrix<float> Y = new Matrix<float>(new Size(matY.Width, matY.Height));
                   matX.CopyTo(X);
                   matY.CopyTo(Y);
                   Matrix<int> yAsInteger =  Y.Convert<int>();
                 TrainData trainData = new TrainData(X, Emgu.CV.ML.MlEnum.DataLayoutType.RowSample, yAsInteger);
                    
                if(radioButtonViablesAndNon.Checked){    
                
                    using(SVM model = new SVM()){
                        model.SetKernel(SVM.SvmKernelType.Linear);
                        model.C = 1;
                        //model.SetKernel(SVM.SvmKernelType.Poly);    
                        //model.Degree = 1.0;
                        model.Type = SVM.SvmType.CSvc;                        
                        //model.Nu = 0.0001;
                        //model.Coef0 = 0.001;
                        model.TermCriteria = new MCvTermCriteria(500, Single.Epsilon);
                        model.TrainAuto(trainData, 10);
                        string path = Path.Combine(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "pollenModel.xml");
                        if (File.Exists(path)) File.Delete(path);
                        FileStorage fs = new FileStorage(path, FileStorage.Mode.Write);
                        model.Write(fs);
                        String output = fs.ReleaseAndGetString();
                        MessageBox.Show("model already updated", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        //Matrix<float> predicted = Y.CopyBlank();
                        //for (int i = 0; i < samples.Rows; i++) {                
                        //        float predictedValue = model.Predict(matX.Row(i));
                        //        Console.WriteLine(predictedValue);
                        //        predicted[i, 0] =predictedValue;
                        // }                     
                        //double rate = evaluate(Y, predicted, ValidationType.HITS);
                        //Console.WriteLine(String.Format("accurracy: {0}", rate));                    
                        X.Dispose();
                        Y.Dispose();
                        yAsInteger.Dispose();

                    }
                } else {
                  using(SVM model = new SVM()){
                        model.SetKernel(SVM.SvmKernelType.Linear);
                        model.C = 1;
                        //model.SetKernel(SVM.SvmKernelType.Poly);    
                        //model.Degree = 1.0;
                        model.Type = SVM.SvmType.CSvc;                        
                        //model.Nu = 0.0001;
                        //model.Coef0 = 0.001;
                        model.TermCriteria = new MCvTermCriteria(1000, Single.Epsilon);
                        model.TrainAuto(trainData, 10);
                        string path = Path.Combine(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "pollenModelViablesAndInterm.xml");
                        if (File.Exists(path)) File.Delete(path);
                        FileStorage fs = new FileStorage(path, FileStorage.Mode.Write);
                        model.Write(fs);
                        String output = fs.ReleaseAndGetString();
                        MessageBox.Show("model already updated", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        //Matrix<float> predicted = Y.CopyBlank();
                        //for (int i = 0; i < samples.Rows; i++) {                
                        //        float predictedValue = model.Predict(matX.Row(i));
                        //        Console.WriteLine(predictedValue);
                        //        predicted[i, 0] =predictedValue;
                        // }                     
                        //double rate = evaluate(Y, predicted, ValidationType.HITS);
                        //Console.WriteLine(String.Format("accurracy: {0}", rate));                    
                        X.Dispose();
                        Y.Dispose();
                        yAsInteger.Dispose();

                    }
                }
            } else {
                MessageBox.Show("You must select the data file","", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}