﻿namespace phenotypingTool.beans.pollen {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnExportToCsv = new System.Windows.Forms.Button();
            this.txtColorImagesPath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBrowseFolderColorImages = new System.Windows.Forms.Button();
            this.flpImages = new System.Windows.Forms.FlowLayoutPanel();
            this.contextMenuImage = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.trainToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.areaHistogramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shapeHistogramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkBoxIntermediates = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.contextMenuImage.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.flpImages);
            this.splitContainer1.Size = new System.Drawing.Size(1004, 414);
            this.splitContainer1.SplitterDistance = 125;
            this.splitContainer1.SplitterWidth = 2;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxIntermediates);
            this.groupBox1.Controls.Add(this.btnExportToCsv);
            this.groupBox1.Controls.Add(this.txtColorImagesPath);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnBrowseFolderColorImages);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(1004, 125);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Folder";
            // 
            // btnExportToCsv
            // 
            this.btnExportToCsv.Image = global::phenotypingTool.beans.pollen.Properties.Resources._014_excel;
            this.btnExportToCsv.Location = new System.Drawing.Point(424, 39);
            this.btnExportToCsv.Margin = new System.Windows.Forms.Padding(2);
            this.btnExportToCsv.Name = "btnExportToCsv";
            this.btnExportToCsv.Size = new System.Drawing.Size(55, 47);
            this.btnExportToCsv.TabIndex = 3;
            this.btnExportToCsv.UseVisualStyleBackColor = true;
            this.btnExportToCsv.Click += new System.EventHandler(this.btnExportToCsv_Click);
            // 
            // txtColorImagesPath
            // 
            this.txtColorImagesPath.Location = new System.Drawing.Point(110, 52);
            this.txtColorImagesPath.Margin = new System.Windows.Forms.Padding(2);
            this.txtColorImagesPath.Name = "txtColorImagesPath";
            this.txtColorImagesPath.ReadOnly = true;
            this.txtColorImagesPath.Size = new System.Drawing.Size(252, 20);
            this.txtColorImagesPath.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 58);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "Images Directory: ";
            // 
            // btnBrowseFolderColorImages
            // 
            this.btnBrowseFolderColorImages.Image = global::phenotypingTool.beans.pollen.Properties.Resources._015_image;
            this.btnBrowseFolderColorImages.Location = new System.Drawing.Point(368, 39);
            this.btnBrowseFolderColorImages.Margin = new System.Windows.Forms.Padding(2);
            this.btnBrowseFolderColorImages.Name = "btnBrowseFolderColorImages";
            this.btnBrowseFolderColorImages.Size = new System.Drawing.Size(52, 47);
            this.btnBrowseFolderColorImages.TabIndex = 0;
            this.btnBrowseFolderColorImages.UseVisualStyleBackColor = true;
            this.btnBrowseFolderColorImages.Click += new System.EventHandler(this.btnBrowseFolder_Click);
            // 
            // flpImages
            // 
            this.flpImages.AutoScroll = true;
            this.flpImages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpImages.Location = new System.Drawing.Point(0, 0);
            this.flpImages.Margin = new System.Windows.Forms.Padding(2);
            this.flpImages.Name = "flpImages";
            this.flpImages.Size = new System.Drawing.Size(1004, 287);
            this.flpImages.TabIndex = 0;
            // 
            // contextMenuImage
            // 
            this.contextMenuImage.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trainToolStripMenuItem,
            this.areaHistogramToolStripMenuItem,
            this.shapeHistogramToolStripMenuItem});
            this.contextMenuImage.Name = "contextMenuImage";
            this.contextMenuImage.Size = new System.Drawing.Size(166, 70);
            // 
            // trainToolStripMenuItem
            // 
            this.trainToolStripMenuItem.Name = "trainToolStripMenuItem";
            this.trainToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.trainToolStripMenuItem.Text = "Train";
            this.trainToolStripMenuItem.Click += new System.EventHandler(this.trainToolStripMenuItem_Click);
            // 
            // areaHistogramToolStripMenuItem
            // 
            this.areaHistogramToolStripMenuItem.Name = "areaHistogramToolStripMenuItem";
            this.areaHistogramToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.areaHistogramToolStripMenuItem.Text = "Area Histogram";
            this.areaHistogramToolStripMenuItem.Visible = false;
            this.areaHistogramToolStripMenuItem.Click += new System.EventHandler(this.areaHistogramToolStripMenuItem_Click);
            // 
            // shapeHistogramToolStripMenuItem
            // 
            this.shapeHistogramToolStripMenuItem.Name = "shapeHistogramToolStripMenuItem";
            this.shapeHistogramToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.shapeHistogramToolStripMenuItem.Text = "Shape Histogram";
            this.shapeHistogramToolStripMenuItem.Visible = false;
            this.shapeHistogramToolStripMenuItem.Click += new System.EventHandler(this.shapeHistogramToolStripMenuItem_Click);
            // 
            // checkBoxIntermediates
            // 
            this.checkBoxIntermediates.AutoSize = true;
            this.checkBoxIntermediates.Location = new System.Drawing.Point(166, 77);
            this.checkBoxIntermediates.Name = "checkBoxIntermediates";
            this.checkBoxIntermediates.Size = new System.Drawing.Size(127, 18);
            this.checkBoxIntermediates.TabIndex = 4;
            this.checkBoxIntermediates.Text = "Include Intermediates";
            this.checkBoxIntermediates.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 414);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.contextMenuImage.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtColorImagesPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBrowseFolderColorImages;
        private System.Windows.Forms.FlowLayoutPanel flpImages;
        private System.Windows.Forms.ContextMenuStrip contextMenuImage;
        private System.Windows.Forms.ToolStripMenuItem trainToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem areaHistogramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shapeHistogramToolStripMenuItem;
        private System.Windows.Forms.Button btnExportToCsv;
        private System.Windows.Forms.CheckBox checkBoxIntermediates;
    }
}