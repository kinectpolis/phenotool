﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Accord.Imaging;

namespace phenotypingTool.beans.pollen {
    class PollenGrainsFilter : IBlobsFilter {
        public bool Check(Blob blob) {
            bool result = false;
            if(blob.Image != null) {
               VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();                                                                               
               Image<Bgr, Byte> color = new Image<Bgr, byte>(blob.Image.ToManagedImage());                                          
               Image<Gray, Byte> gray = color.Convert<Gray, Byte>();                                                  
               Image<Gray, Byte> mask = gray.CopyBlank();
               CvInvoke.Threshold(gray, mask,0,255, Emgu.CV.CvEnum.ThresholdType.Binary);                        
               CvInvoke.FindContours(mask.Clone(), contours, null, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);                                    
                if(contours.Size > 0) {   
                    using (VectorOfPoint contour = contours[0]){
                        double area = CvInvoke.ContourArea(contours, false);
                        double perimeter = CvInvoke.ArcLength(contour, true);
                        double shape = (4 * Math.PI * area) / Math.Pow(perimeter, 2);                        
                            if(shape > 0.5) {
                                result = true;
                            }
                    }         
               }
                
               color.Dispose();
               gray.Dispose();
               mask.Dispose();
               contours.Dispose();
            }
            return result;
        }
    }
}
