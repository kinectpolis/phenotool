﻿using Accord.Imaging;
using Accord.Imaging.Filters;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using phenotypingTool.common;
using phenotypingTool.cv;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace phenotypingTool.beans.pollen {
    public class PollenGrainsPhoto : Photo
    {
        public List<PollenGrain> pollenGrains;
        private String debugFolder;


        public PollenGrainsPhoto(string imagePath, int w=1024, int h=780){
            try{
            this.path = imagePath;
            this.color = new Image<Bgr, byte>(imagePath);
            this.images = new Dictionary<string, Bitmap>();
            this.pollenGrains = new List<PollenGrain>();
            this.color = this.color.Resize(w, h, Emgu.CV.CvEnum.Inter.Linear, true);
            this.debugFolder = Path.Combine(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "debugPollen");
            if(!Directory.Exists(this.debugFolder)){
                Directory.CreateDirectory(this.debugFolder);            
            }
            }
            catch(Exception ex){
                throw new Exception(String.Format("Error reading the file {0} : {1}", Path.GetFileNameWithoutExtension(imagePath), ex));
            }
        }

        private Task findContourns(Image<Gray, Byte> src, int areaTreshold = 80, int widthTreshold = 600){
            #region fillGranes
            return Task.Run(delegate (){
                VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();                
                Mat hierarchy = new Mat();
                CvInvoke.FindContours(src.Clone(), contours, hierarchy, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
                //Matrix<int> matrix = new Matrix<int>(hierarchy.Rows, hierarchy.Cols,hierarchy.NumberOfChannels);
                //int[,] data = matrix.Data;
                //hierarchy.CopyTo(matrix);                
                int contoursCount = contours.Size;               
                for (int i = 0; i < contoursCount; i++){
                    using (VectorOfPoint contour = contours[i]){
                        double area = CvInvoke.ContourArea(contours[i], false);
                        //double perimeter = CvInvoke.ArcLength(contour, true);
                        //double shape = (4 * Math.PI * area) / Math.Pow(perimeter, 2);
                        //PointF[] hull = CvInvoke.ConvexHull(contour.ToArray().Select(pt=> new PointF(pt.X, pt.Y)).ToArray());
                        Rectangle bounds = CvInvoke.BoundingRectangle(contour);
                        //int[] data = cvUtilities.GetContournHierarchy(hierarchy, i);
                        //Console.WriteLine(data.Length);
                        if(area > areaTreshold /* && CvInvoke.IsContourConvex(contours[i])*/) {
                            CvInvoke.DrawContours(this.mask, contours, i, new MCvScalar(255), -1);
                            //CvInvoke.DrawContours(this.color, contours, i, new MCvScalar(255,0,255), 1);
                            //CvInvoke.PutText(this.color,String.Format("{0:0.##}", shape), bounds.Location, Emgu.CV.CvEnum.FontFace.HersheySimplex, 0.3 , new MCvScalar(0,0,0));
                                /*using(VectorOfPoint approxCurve = new VectorOfPoint()) {
                                    CvInvoke.ApproxPolyDP(contour, approxCurve, Single.Epsilon, true);
                                    this.color.DrawPolyline(approxCurve.ToArray(), true, new Bgr(255, 0, 255), 1);
                                }*/

                        }
                        else{
                            CvInvoke.DrawContours(this.mask, contours, i, new MCvScalar(0), -1);                            
                        }      
                    }
                }                
                contours.Dispose();                

            });
            #endregion

        }
        public override async Task extractBackground(){
            //return Task.Run(delegate(){
            Image<Gray, Byte> gray = this.color.Convert<Gray, Byte>();
            this.mask = gray.CopyBlank();
            Image<Gray, Byte> canny = gray.CopyBlank();
            Image<Gray, float> sobX = gray.Sobel(1,0, 3);
            Image<Gray, float> sobY = gray.Sobel(0,1, 3);
            sobX =  sobX.AbsDiff(new Gray(0));
            sobY =  sobY.AbsDiff(new Gray(0));
            Image<Gray, float> borders = sobX + sobY;
            gray = borders.Convert<Gray, Byte>();
            //this.images.Add("laplace", gray.ToBitmap());
            CvInvoke.Canny(gray, canny, 20, 200);            
            Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Ellipse, new Size(3, 3), new Point(-1, -1));
            canny._MorphologyEx(Emgu.CV.CvEnum.MorphOp.Close, kernel, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(1.0));
            //kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Ellipse, new Size(3, 3), new Point(-1, -1));
            //canny._MorphologyEx(Emgu.CV.CvEnum.MorphOp.Dilate, kernel, new Point(-1, -1), 1, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(1.0));
            await findContourns(canny);
            gray.Dispose();
            borders.Dispose();
            canny.Dispose();     
            sobX.Dispose();
            sobY.Dispose();
            //});
        }

        public override Task extractContourns(){
            return Task.Run(delegate (){
                Bitmap maskAsBitmap =mask.ToBitmap();
                var dt = new BinaryWatershed(0.5f, DistanceTransformMethod.Euclidean);
                Bitmap output = dt.Apply(maskAsBitmap);
                BlobCounter bc = new BlobCounter( );
                bc.ObjectsOrder = ObjectsOrder.Area;
                //bc.FilterBlobs = true;
                //bc.BlobsFilter = new PollenGrainsFilter();
                bc.ProcessImage( output );  
                Blob[] blobs = bc.GetObjectsInformation(); 

                Bitmap colorAsBitmap = new Bitmap(this.color.ToBitmap());
                foreach(Blob blob in blobs) {
                    //Accord.Net
                     List<Accord.IntPoint> border= bc.GetBlobsEdgePoints(blob);
                     Point[] borderPoints = border.Select(pt => new Point(pt.X, pt.Y)).ToArray();
                     bc.ExtractBlobsImage(colorAsBitmap, blob, false);                     
                     /*var marker = new PointsMarker(border, Color.Lime, 1);
                     marker.ApplyInPlace(colorAsBitmap);*/                     
                     //EmguCV
                     Image<Bgr, Byte> blobTemp = new Image<Bgr, byte>(blob.Image.ToManagedImage());                                          
                     Image<Gray, Byte> blobTempMask = blobTemp.Convert<Gray, Byte>(); 
                     CvInvoke.Threshold(blobTempMask, blobTempMask,0,255, Emgu.CV.CvEnum.ThresholdType.Binary);                        
                     //blobTempMask.Save(String.Format("{0}/{1}.jpg", this.debugFolder, Guid.NewGuid().ToString()));
                     VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();                                                          
                     CvInvoke.FindContours(blobTempMask.Clone(), contours, null, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);                                    
                     if(contours.Size > 0) {
                        using (VectorOfPoint contour = contours[0]){
                            Rectangle bounds = CvInvoke.BoundingRectangle(contour);
                            double area = CvInvoke.ContourArea(contours[0], false);
                            double perimeter = CvInvoke.ArcLength(contour, true);
                            double shape = (4 * Math.PI * area) / Math.Pow(perimeter, 2);
                            //this.color.Draw(String.Format("{0:0.##}", shape), blob.Rectangle.Location, Emgu.CV.CvEnum.FontFace.HersheyPlain,1,new Bgr(0,255,0),2);
                             if(shape >= .5 && area >= 100 && contour.Size > 5){
                                //this.color.Draw(new Cross2DF(new PointF(blob.CenterOfGravity.X,blob.CenterOfGravity.Y) ,15, 15), new Bgr(0,255,0),1);
                                CircleF circle = CvInvoke.MinEnclosingCircle(contour);                            
                                RotatedRect ellipse = CvInvoke.FitEllipse(contour);
                                double eccentricity = ellipse.Size.Height/ellipse.Size.Width;
                                float width = ellipse.Size.Width;
                                float height = ellipse.Size.Height;
                                Image<Hsv, Byte> hsv = blobTemp.Convert<Hsv, Byte>();
                                Image<Lab, Byte> lab = blobTemp.Convert<Lab, Byte>();
                                Image<Hls, Byte> hls = blobTemp.Convert<Hls, Byte>();
                                Image<Gray, Byte> gray = blobTemp.Convert<Gray, Byte>();
                                double homogeneity = gray.return_homogeneity();
                                double contrast = gray.return_contrast();
                                double energy = gray.return_energy();
                                // Create a new Local Binary Pattern with default values:
                                //var lbp = new LocalBinaryPattern(blockSize: 3, cellSize: 0, normalize: true); 
                                ///*Now those descriptors can be used to represent the image itself, such
                                // as for example, in the Bag-of-Visual-Words approach for classification.*/
                                //IEnumerable<FeatureDescriptor> descriptors = lbp.Transform(gray.ToBitmap());
                                //Console.WriteLine(descriptors.Count());
                                // Create a new gray-level cooccurrence matrix using default parameters
                                // Ensure results are reproducible
                                //Accord.Math.Random.Generator.Seed = 0;
                                //// Create a new Haralick extractor:
                                //Haralick haralick = new Haralick() {
                                //    Mode = HaralickMode.AverageWithRange,
                                //    CellSize = 0 // use the entire image
                                //};
                                //// Extract the descriptors from the texture image
                                //IEnumerable<FeatureDescriptor> descriptors = haralick.Transform(gray.ToBitmap());
                                ////// If desired, we can obtain the GLCM for the image:
                                ////GrayLevelCooccurrenceMatrix glcm = haralick.Matrix;
                                
                                // Create a new gray-level cooccurrence matrix using default parameters
                               
                                var glcm = new GrayLevelCooccurrenceMatrix(distance: 1, degree: CooccurrenceDegree.Degree0, normalize: true);
                               // Extract the matrix from the image
                                double[,] matrix = glcm.Compute(gray.ToBitmap());
                                HaralickDescriptor haralick = new HaralickDescriptor(matrix);
                                double[] haralickFeatures =  haralick.GetVector();
                                //Console.WriteLine(haralickFeatures.Length);
                                

                                    this.pollenGrains.Add(new PollenGrain(blob){ 
                                       area = area,
                                       shape = shape,
                                       perimeter = perimeter,
                                       npoints = contour.Size,
                                       width = bounds.Width,
                                       height = bounds.Height,
                                       centroideY = circle.Center.Y,
                                       centroideX = circle.Center.X,
                                       radius = circle.Radius,
                                       aspectRatio = width / height,
                                       extent = area / (width * height),
                                       homogeneity = haralick.AngularSecondMomentum,
                                       contrast = haralick.Contrast,
                                       haralickFeatures = haralickFeatures,
                                        hsvAvg = new MCvScalar() {
                                            V0 = hsv.Data[(int)circle.Center.Y, (int)circle.Center.X, 0],
                                            V1 = hsv.Data[(int)circle.Center.Y, (int)circle.Center.X, 1],
                                            V2 = hsv.Data[(int)circle.Center.Y, (int)circle.Center.X, 2]
                                        },
                                        bgrAvg = new MCvScalar() {
                                            V0 = blobTemp.Data[(int)circle.Center.Y, (int)circle.Center.X, 0],
                                            V1 = blobTemp.Data[(int)circle.Center.Y, (int)circle.Center.X, 1],
                                            V2 = blobTemp.Data[(int)circle.Center.Y, (int)circle.Center.X, 2]
                                        }
                                        //hsvAvg = hsv.GetAverage().MCvScalar,
                                        //bgrAvg = blobTemp.GetAverage().MCvScalar
                                    });

                                hsv.Dispose();
                                lab.Dispose();
                                hls.Dispose();
                                gray.Dispose();
                                 
                             }
                            }    
                        
                            //if(shape > 0.5) {
                            //    Brush brush = new SolidBrush(Color.FromArgb(80, 0, 255, 0));
                            //    Pen pen = new Pen(brush);
                            //    this.pollenGrains.Add(new PollenGrain(blob));
                            //    Graphics g = Graphics.FromImage(colorAsBitmap);
                            //    g.SmoothingMode = SmoothingMode.AntiAlias;
                            //    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                            //    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                            //    g.DrawLines(pen, borderPoints);
                            //    g.DrawString(String.Format("{0:0.##}", shape), new Font("Tahoma", 8), Brushes.Red, blob.Rectangle.Center());
                            //    g.Flush();
                            //    g.Dispose();
                            //}
                     }         
                     
                     blobTemp.Dispose();  
                     contours.Dispose();                     
                     blobTempMask.Dispose();                                              
                }                           
                //this.color = new Image<Bgr, byte>(colorAsBitmap); 
                
            });
        }

       
        public override async Task process(){
            await this.extractBackground();
            await this.extractContourns();
            //double[] data = this.pollenGrains.Select(grain => Convert.ToDouble(grain.blob.Area)).ToArray();
            //double upperArea = data.Min();//data.Percentile(95);//data.UpperQuartile();
            //double lowerArea = data.Max(); //data.Percentile(25);//data.UpperQuartile();
            //double medianArea = data.Median();
            //data = this.pollenGrains.Select(grain => grain.shape).ToArray();
            //double lowerShape = data.Percentile(2);//data.UpperQuartile();
            //double medianShape = data.Median();
            //this.color.Draw(String.Format("{0:0.##}", medianShape), new Point(50, 50), Emgu.CV.CvEnum.FontFace.HersheyPlain, 1, new Bgr(255, 0, 255));
            
        }
    }
}
