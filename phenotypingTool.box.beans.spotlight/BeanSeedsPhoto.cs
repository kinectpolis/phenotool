﻿using phenotypingTool.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using phenotypingTool.cv;
using System.Drawing;
using Accord.Imaging;
using Accord.Imaging.Filters;
using System.IO;
using Emgu.CV.Util;

namespace phenotypingTool.box.beans.spotlight
{
    public struct Seed{
        public Bitmap image;
        public string fileName;
        public int id;      
        public Rectangle bounds;
        public Point spotLigthLocation;
        public double area;
        public List<Accord.IntPoint> contourn;
    }
    
    public class BeanSeedsPhoto : Photo
    {
        public Image<Bgr, byte> src;
        public Tuple<string, Image<Bgr, byte>>[] views;        
        private Point offset;
        private Rectangle roi;
        private Size preferredScale;
        public String debugFolder;
        public List<Seed> seeds;
      
        public BeanSeedsPhoto(Image<Bgr, byte> src, Tuple<string, Image<Bgr, byte>>[] views){
            this.images = new Dictionary<string, Bitmap>();
            this.src = src;
            this.views = views;
            preferredScale = new Size(1024, 780);
            this.debugFolder = Path.Combine(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "debug");            
            if(!Directory.Exists(this.debugFolder))
                Directory.CreateDirectory(this.debugFolder);             
            
        }

        public override Task extractBackground(){
           return Task.Run(async delegate (){
            this.src = await cvUtilities.correctLentDistorsion(this.src,1.25f);
            this.src = this.src.Resize(preferredScale.Width, preferredScale.Height, Emgu.CV.CvEnum.Inter.Linear, true);
            Size sz = this.src.Size;
            this.offset = new Point(80, 40);
            this.roi = new Rectangle(95,60, sz.Width - this.offset.X - 120, sz.Height - this.offset.Y - 110); 
            this.src = this.src.Copy(roi);                 
            Image<Gray, Byte> gray = this.src.Convert<Gray, Byte>();
            CvInvoke.CLAHE(gray, 1.0, new Size(2, 2), gray);
            this.mask = gray.CopyBlank();
            CvInvoke.Threshold(gray, this.mask, 0, 255, Emgu.CV.CvEnum.ThresholdType.Otsu | Emgu.CV.CvEnum.ThresholdType.Binary);
            Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Ellipse, new Size(3, 3), new Point(-1, -1));
            mask._MorphologyEx(Emgu.CV.CvEnum.MorphOp.Open, kernel, new Point(-1, -1), 10, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(1.0));
            gray.Dispose();
            });  
        }

        public override Task extractContourns(){
           return Task.Run(async delegate (){
            //get input image to apply watershed                            
            UnmanagedImage input = UnmanagedImage.FromManagedImage(this.mask.ToBitmap());
            var dt = new BinaryWatershed(0.7f, DistanceTransformMethod.Euclidean);
            Bitmap output = dt.Apply(input.ToManagedImage());
            BlobCounter bc = new BlobCounter( );                
            bc.ProcessImage( output );
            Blob[] blobs = bc.GetObjectsInformation();           
            foreach(var value in this.views){
              Image<Bgr,Byte> v = value.Item2.Copy();
               v = v.Resize(preferredScale.Width, preferredScale.Height, Emgu.CV.CvEnum.Inter.Linear, true);
               v = await cvUtilities.correctLentDistorsion(v,1.25f);                   
               v = v.Copy(this.roi);             
               Image<Lab, Byte> colorModel = v.Convert<Lab, Byte>();
               Image<Gray, Byte>[] channels = colorModel.Split();               
               Image<Gray, Byte> ch = channels[0];
               //ch._EqualizeHist();                   
                //Bitmap bmp = ch.ToBitmap();//channels[0].ToBitmap();
                Image<Bgr, Byte> chAsColor = ch.Convert<Bgr, Byte>();
                UnmanagedImage result = UnmanagedImage.FromManagedImage(chAsColor.ToBitmap());
                  foreach(Blob blob in blobs){
                     //Console.WriteLine(blob.ID);
                     List<Accord.IntPoint> border= bc.GetBlobsEdgePoints(blob);
                     Point[] points = border.Select(pt => new Point(pt.X, pt.Y)).ToArray(); 
                     //blob.Image.ToManagedImage().Save(String.Format("{0}.jpg", Guid.NewGuid().ToString()));                                          
                      bc.ExtractBlobsImage(ch.ToBitmap(), blob, true);
                       //OtsuThreshold otsu = new OtsuThreshold( );
                       //otsu.ApplyInPlace( blob.Image );
                       //byte t = otsu.ThresholdValue;
                            
                           Image<Gray,Byte> seed = new Image<Gray,Byte>(blob.Image.ToManagedImage());
                           Image<Bgr,Byte> seedColor = seed.Convert<Bgr,Byte>();
                           double min = 0;
                           double max = 0;
                           Point minLoc = new Point();
                           Point maxLoc = new Point();
                           CvInvoke.MinMaxLoc(seed,ref min, ref max, ref minLoc, ref maxLoc);
                           //seedColor.Draw(new CircleF( new PointF(maxLoc.X, maxLoc.Y),3),new Bgr(255,0,255), -1);
                           //seedColor.Save(String.Format("{0}/{1}.jpg",this.debugFolder, Guid.NewGuid().ToString()));                            
                            Image<Bgr,Byte> resultAsEmguCvImage = new Image<Bgr, byte>(result.ToManagedImage());              
                            resultAsEmguCvImage.Draw(new Cross2DF(maxLoc,10,10), new Bgr(0,255,0),1);
                            //resultAsEmguCvImage.FillConvexPoly(points, new Bgr(0,255,20), Emgu.CV.CvEnum.LineType.EightConnected);                            
                            VectorOfPoint contour = new VectorOfPoint(points); 
                            double area= blob.Area;
                            double perimeter = contour.Size;
                            double shape = 4 * Math.PI * (area / Math.Pow(perimeter, 2));
                            CircleF circle = CvInvoke.MinEnclosingCircle(contour); 
                            RotatedRect ellipse = CvInvoke.FitEllipse(contour);
                            //resultAsEmguCvImage.Draw(new Ellipse(ellipse),new Bgr(0,255,0));
                            //resultAsEmguCvImage.Draw(ellipse.MinAreaRect(),new Bgr(0,255,0));
                            resultAsEmguCvImage.Draw(String.Format("{0:0.##}", area),new Point((int)circle.Center.X,(int)circle.Center.Y),  Emgu.CV.CvEnum.FontFace.HersheyPlain, 0.6, new Bgr(0,255,255));
                            
                            //resultAsEmguCvImage.Draw(circle,new Bgr(0,255,255));
                            //Drawing.Rectangle(bmp,new Rectangle(maxLoc, new Size(4,4)),Color.Lime);                 
                            result = UnmanagedImage.FromManagedImage(resultAsEmguCvImage.ToBitmap());
                            resultAsEmguCvImage.Dispose();
                        
                           seed.Dispose();
                           seedColor.Dispose();
                           

                            this.seeds.Add(
                                new Seed(){
                                   area= blob.Area,
                                   image = blob.Image.ToManagedImage(),
                                   fileName = Path.GetFileNameWithoutExtension(value.Item1),
                                   id = blob.ID,
                                   bounds = blob.Rectangle,
                                   spotLigthLocation = maxLoc,
                                   contourn = border
                                }        
                           );
                       
                       //IterativeThreshold threshold = new IterativeThreshold( 250, 255 );
                      //threshold.ApplyInPlace( blob.Image );
                      //blob.Image.ToManagedImage().Save(String.Format("{0}/{1}.jpg",this.debugFolder, Guid.NewGuid().ToString()));
                       var marker = new PointsMarker(border, Color.LightGreen, 3);
                       marker.ApplyInPlace(result);
                      
                      
                  }
                  this.images.Add(value.Item1, result.ToManagedImage());
                  v.Dispose();
                  colorModel.Dispose();
                  channels = null;
             }                      

            
           });           
        }

        public override async Task process(){  
            this.seeds = new List<Seed>();
            await this.extractBackground();
            await this.extractContourns();
        }
    }
}
