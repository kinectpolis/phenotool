﻿using Accord.Imaging;
using Accord.Imaging.Filters;
using Emgu.CV;
using Emgu.CV.Structure;
using phenotypingTool.common;
using phenotypingTool.cv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace phenotypingTool.box.beans.spotlight
{
    public partial class MainForm : Form
    {
        private readonly SynchronizationContext synchronizationContext;
        private Emgu.CV.UI.PanAndZoomPictureBox  viewer;
        public MainForm()
        {
            InitializeComponent();
            this.synchronizationContext = SynchronizationContext.Current;                   
            viewer = new Emgu.CV.UI.PanAndZoomPictureBox();
            viewer.Dock = DockStyle.Fill;     
            //viewer.SizeMode = PictureBoxSizeMode.CenterImage;             
            this.pnlMain.Controls.Add(viewer);
        }

        private void btnBrowseGray_Click(object sender, EventArgs e)
        {
             try{
                OpenFileDialog dialog = new OpenFileDialog();
                if (dialog.ShowDialog() == DialogResult.OK){
                    this.txtGrayImage.Text = dialog.FileName;
                    this.txtGrayImage.Enabled = false;                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBrowseFolder_Click(object sender, EventArgs e)
        {
              try
            {
                
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                if (dialog.ShowDialog() == DialogResult.OK){
                    this.txtImagesFolder.Text = dialog.SelectedPath;
                    this.txtImagesFolder.Enabled = false;                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void btnProcess_Click(object sender, EventArgs e){

            if(File.Exists(txtGrayImage.Text) && Directory.Exists(txtImagesFolder.Text)){
                Image<Bgr, byte> src =  await Task.Run<Image<Bgr, byte>>(delegate (){ return new Image<Bgr, byte>(this.txtGrayImage.Text);});
                Tuple<String,Image<Bgr, byte>>[] images = await Task.WhenAll(Directory.EnumerateFiles(txtImagesFolder.Text).Select(imagePath=> Task.Run(delegate(){   return new Tuple<String,Image<Bgr, byte>>(Path.GetFileName(imagePath), new Image<Bgr,Byte>(imagePath)); }) ));
                BeanSeedsPhoto bsp= new BeanSeedsPhoto(src, images);
                await bsp.process();
                this.pnlImages.Controls.Clear();
                foreach(var img in bsp.images){
                    PictureBox pbox = this.createPictureBox(img.Value);
                    this.pnlImages.Controls.Add(pbox);                
                }
                string pathCsv = Path.Combine(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "result.csv");
                using (var csv = new StreamWriter(pathCsv, true)){               
                var query = from seed in bsp.seeds group  seed by seed.id; 
                foreach (var group in query){
                     csv.WriteLine(String.Format("Bean {0}", group.Key));
                     csv.WriteLine("File, X, Y");
                     // Explicit type for student could also be used here.
                         foreach (var seed in group){
                             csv.WriteLine("{0},{1},{2}", seed.fileName,  seed.spotLigthLocation.X, seed.spotLigthLocation.Y);
                         }
                 }
                 csv.Flush();
                }
                MessageBox.Show("done", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }


       private PictureBox createPictureBox(Bitmap img, Object tag = null)
        {
            #region make Picture Box
            PictureBox pbx = new PictureBox();
            pbx.Image = img;
            pbx.Size = new Size(150, 150);
            pbx.BorderStyle = BorderStyle.FixedSingle;
            pbx.Margin = new Padding(10);
            pbx.Cursor = Cursors.Hand;
            pbx.Tag = tag;
            pbx.SizeMode = PictureBoxSizeMode.Zoom;
            //pbx.Dock = DockStyle.Top;
            pbx.DoubleClick += displayImage_PictureBox_Click;
            //img.Dispose();
            return pbx;
            #endregion
        }

        private void displayImage_PictureBox_Click(object sender, EventArgs e){
            PictureBox pbox = sender as PictureBox;
            Image<Bgr, Byte> imageClicked = new Image<Bgr, Byte>(pbox.Image as Bitmap);
            //Emgu.CV.UI.ImageViewer viewer = new Emgu.CV.UI.ImageViewer(imageClicked);
            //viewer.ShowDialog();
            this.viewer.Image = imageClicked.ToBitmap();
            this.viewer.Update();

        }
          
    }
}
