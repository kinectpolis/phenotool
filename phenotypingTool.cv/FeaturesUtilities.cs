﻿using Accord.Imaging;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace phenotypingTool.cv {



    public static class FeaturesUtilities {
        public static double return_energy(this Image<Gray, Byte> image) {
            double energy = 0;
            Image<Gray, float> imageAsfloat = image.Convert<Gray, float>();
            for(int i = image.Rows - 1; i >= 0; i--) {
                for(int j = image.Cols - 1; j >= 0; j--) {
                    energy = energy + Math.Pow(imageAsfloat[i, j].Intensity, 2);
                }
            }
            imageAsfloat.Dispose();
            return energy;
        }

        public static double return_homogeneity(this Image<Gray, Byte> image) {
            double homogeneity = 0;
            Image<Gray, float> imageAsfloat = image.Convert<Gray, float>();
            for(int i = image.Rows - 1; i >= 0; i--) {
                for(int j = image.Cols - 1; j >= 0; j--) {
                    homogeneity = homogeneity + (imageAsfloat[i, j].Intensity / (1 + Math.Abs(i - j)));
                }
            }
            imageAsfloat.Dispose();
            return homogeneity;
        }

        public static double return_contrast(this Image<Gray, Byte> image) {
            double contrast = 0;
            Image<Gray, float> imageAsfloat = image.Convert<Gray, float>();
            for(int i = image.Rows - 1; i >= 0; i--) {
                for(int j = image.Cols - 1; j >= 0; j--) {
                    contrast = contrast + (Math.Pow((i - j), 2) * imageAsfloat[i, j].Intensity);
                }
            }
            return contrast;
        }

        public static float[] histogram_to_vector_feature(Image<Bgr, byte> src, Image<Gray, byte> mask) {

            Image<Lab, Byte> dst = src.Convert<Lab, Byte>();
            DenseHistogram histo = new DenseHistogram(255, new RangeF(0, 255));
            Image<Gray, Byte>[] channels = dst.Split();

            histo.Calculate(new Image<Gray, Byte>[] { channels[0] }, true, mask);
            CvInvoke.Normalize(histo, histo);
            MatND<float> mat = new MatND<float>(255);
            histo.CopyTo(mat);
            float[] binValuesB = (float[])mat.ManagedArray;
            histo.Clear();


            histo.Calculate(new Image<Gray, Byte>[] { channels[1] }, true, mask);
            CvInvoke.Normalize(histo, histo);
            mat = new MatND<float>(255);
            histo.CopyTo(mat);
            float[] binValuesG = (float[])mat.ManagedArray;
            histo.Clear();

            histo.Calculate(new Image<Gray, Byte>[] { channels[2] }, true, mask);
            CvInvoke.Normalize(histo, histo);
            mat = new MatND<float>(255);
            histo.CopyTo(mat);
            float[] binValuesR = (float[])mat.ManagedArray;
            histo.Clear();

            return binValuesB.Concat(binValuesG).Concat(binValuesR).ToArray();

        }

        public static float[] image_to_vector_feature(Image<Bgr, byte> src, int w = 20, int h = 20) {

            Image<Bgr, float> copy = src.Convert<Bgr, float>();
            copy = copy.Resize(w, h, Emgu.CV.CvEnum.Inter.Linear, false);
            Mat[] channels = copy.Mat.Split();
            int n = copy.Rows * copy.Cols;
            float[] vectorOfFeature = new float[n * 3];
            for(int i = 0; i < channels.Length; i++) {
                Mat c = channels[0].Reshape(1, n);
                MatND<float> temp = new MatND<float>(n);
                c.CopyTo(temp);
                float[] partition = (float[])temp.ManagedArray;
                partition.CopyTo(vectorOfFeature, i * n);
                c.Dispose();
                temp.Dispose();
            }
            channels = null;
            copy.Dispose();
            return vectorOfFeature;
        }

        public static float[] histograms_to_vector_feature(Image<Bgr, byte> src) {
            Point center = new Point((int)(src.Width * 0.5), (int)(src.Height * 0.5));
            Rectangle[] segments = new Rectangle[] {
                new Rectangle(0, 0, center.X, center.Y),
                new Rectangle(center.X, 0, src.Width, center.Y),
                new Rectangle(center.X, center.Y, src.Width, src.Height),
                new Rectangle(0, center.Y, center.X, src.Height),
            };
            Image<Gray, Byte> ellipseMask = new Image<Gray, Byte>(src.Size);
            Size axes = new Size((int)(src.Width * 0.75) / 2, (int)(src.Height * 0.75) / 2);
            CvInvoke.Ellipse(ellipseMask, center, axes, 0, 0, 360, new MCvScalar(255), -1);
            List<float[]> features = new List<float[]>();
            foreach(Rectangle s in segments) {
                Image<Gray, Byte> cornerMask = new Image<Gray, byte>(src.Size);
                CvInvoke.Rectangle(cornerMask, s, new MCvScalar(255), -1);
                CvInvoke.Subtract(cornerMask, ellipseMask, cornerMask);
                features.Add(histogram_to_vector_feature(src, cornerMask));
                cornerMask.Dispose();
            }
            features.Add(histogram_to_vector_feature(src, ellipseMask));
            return String.Join(",", features.Select(histogram => string.Join(",", histogram))).Split(',').Select(el => Convert.ToSingle(el)).ToArray();

        }

        public static float[] segments_to_vector_feature(Image<Bgr, byte> src) {
            Point center = new Point((int)(src.Width * 0.5), (int)(src.Height * 0.5));
            Rectangle[] segments = new Rectangle[] {
                new Rectangle(0, 0, center.X, center.Y),
                new Rectangle(center.X, 0, src.Width, center.Y),
                new Rectangle(center.X, center.Y, src.Width, src.Height),
                new Rectangle(0, center.Y, center.X, src.Height),
            };
            Image<Gray, Byte> ellipseMask = new Image<Gray, Byte>(src.Size);
            Size axes = new Size((int)(src.Width * 0.75) / 2, (int)(src.Height * 0.75) / 2);
            CvInvoke.Ellipse(ellipseMask, center, axes, 0, 0, 360, new MCvScalar(255), -1);
            List<float> features = new List<float>();
            foreach(Rectangle s in segments) {
                Image<Gray, Byte> cornerMask = new Image<Gray, byte>(src.Size);
                CvInvoke.Rectangle(cornerMask, s, new MCvScalar(255), -1);
                CvInvoke.Subtract(cornerMask, ellipseMask, cornerMask);
                features.Add(Convert.ToSingle(src.GetAverage(cornerMask).Red));
                features.Add(Convert.ToSingle(src.GetAverage(cornerMask).Green));
                features.Add(Convert.ToSingle(src.GetAverage(cornerMask).Blue));
                cornerMask.Dispose();
            }
            features.Add(Convert.ToSingle(src.GetAverage(ellipseMask).Red));
            features.Add(Convert.ToSingle(src.GetAverage(ellipseMask).Green));
            features.Add(Convert.ToSingle(src.GetAverage(ellipseMask).Blue));
            return features.ToArray();

        }

        public static float[] image_characteristics_to_feature(Image<Bgr, Byte> src) {
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Image<Gray, Byte> gray = src.Convert<Gray, Byte>();
            Image<Gray, Byte> mask = gray.CopyBlank();
            CvInvoke.Threshold(gray, mask, 0, 255, Emgu.CV.CvEnum.ThresholdType.Binary);
            CvInvoke.FindContours(mask.Clone(), contours, null, Emgu.CV.CvEnum.RetrType.External, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
            List<float> vector_of_features = new List<float>();
            if(contours.Size > 0) {
                using(VectorOfPoint contour = contours[0]) {
                    Rectangle bounds = CvInvoke.BoundingRectangle(contour);
                    CircleF circle = CvInvoke.MinEnclosingCircle(contour);
                    RotatedRect ellipse = CvInvoke.FitEllipse(contour);  
                    float area = Convert.ToSingle(CvInvoke.ContourArea(contour, false));
                    float perimeter = Convert.ToSingle(CvInvoke.ArcLength(contour, true));
                    float shape = Convert.ToSingle((4 * Math.PI * area) / Math.Pow(perimeter, 2));
                    float eccentricity = Convert.ToSingle(ellipse.Size.Height / ellipse.Size.Width);
                    float width = ellipse.Size.Width;
                    float height = ellipse.Size.Height;
                    float equivDiameter = Convert.ToSingle(Math.Sqrt(4 * area / Math.PI));
                    float extent = Convert.ToSingle(area / ((bounds.Height * bounds.Height)));
                    float aspectRatio = width / height;
                    float npoints = Convert.ToSingle(CvInvoke.CountNonZero(mask));
                    PointF[] hull = CvInvoke.ConvexHull(contour.ToArray().Select(pt => new PointF(pt.X, pt.Y)).ToArray());
                    float solidity = 0;
                    using(VectorOfPoint hullContourn = new VectorOfPoint(hull.Select(pt => new Point((int)pt.X, (int)pt.Y)).ToArray())) {
                        solidity = Convert.ToSingle(area / CvInvoke.ContourArea(hullContourn, false));
                    }

                    vector_of_features.AddRange(new float[]{
                            area,
                            perimeter,
                            shape,
                            eccentricity,
                            width,
                            height,
                            equivDiameter,
                            extent,
                            aspectRatio,
                            npoints,
                            solidity,
                            //circle.Radius,
                            Convert.ToSingle(bounds.Width / 2),
                            Convert.ToSingle(bounds.Width / 4),
                            Convert.ToSingle(bounds.Height / 2),
                            Convert.ToSingle(bounds.Height / 4)
                            });
                }

            }
            mask.Dispose();
            gray.Dispose();
            return vector_of_features.ToArray();
        }



        public static float[] image_color_models_to_feature(Image<Bgr, Byte> src) {
            Image<Gray, Byte> gray = src.Convert<Gray, Byte>();
            Image<Gray, Byte> mask = gray.CopyBlank();
            Image<Hsv, float> hsv = src.Convert<Hsv, float>();
            Image<Lab, float> lab = src.Convert<Lab, float>();
            Image<Hls, float> hls = src.Convert<Hls, float>();
            CvInvoke.Threshold(gray, mask, 0, 255, Emgu.CV.CvEnum.ThresholdType.Binary);
            List<float> colorVectorOfFeatures = new List<float>();

            colorVectorOfFeatures.Add(Convert.ToSingle(gray.GetAverage(mask).Intensity));

            colorVectorOfFeatures.Add(Convert.ToSingle(src.GetAverage(mask).Red));
            colorVectorOfFeatures.Add(Convert.ToSingle(src.GetAverage(mask).Blue));
            colorVectorOfFeatures.Add(Convert.ToSingle(src.GetAverage(mask).Green));
            colorVectorOfFeatures.Add(Convert.ToSingle(src.GetSum().Red));
            colorVectorOfFeatures.Add(Convert.ToSingle(src.GetSum().Green));
            colorVectorOfFeatures.Add(Convert.ToSingle(src.GetSum().Blue));

            colorVectorOfFeatures.Add(Convert.ToSingle(hsv.GetAverage(mask).Hue));
            colorVectorOfFeatures.Add(Convert.ToSingle(hsv.GetAverage(mask).Satuation));
            colorVectorOfFeatures.Add(Convert.ToSingle(hsv.GetAverage(mask).Value));
            colorVectorOfFeatures.Add(Convert.ToSingle(hsv.GetSum().Hue));
            colorVectorOfFeatures.Add(Convert.ToSingle(hsv.GetSum().Satuation));
            colorVectorOfFeatures.Add(Convert.ToSingle(hsv.GetSum().Value));


            //colorVectorOfFeatures.Add(Convert.ToSingle(lab.GetAverage(mask).X));
            //colorVectorOfFeatures.Add(Convert.ToSingle(lab.GetAverage(mask).Y));
            //colorVectorOfFeatures.Add(Convert.ToSingle(lab.GetAverage(mask).Z));
            //colorVectorOfFeatures.Add(Convert.ToSingle(lab.GetSum().X));
            //colorVectorOfFeatures.Add(Convert.ToSingle(lab.GetSum().Y));
            //colorVectorOfFeatures.Add(Convert.ToSingle(lab.GetSum().Z));

            //colorVectorOfFeatures.Add(Convert.ToSingle(hls.GetAverage(mask).Hue));
            //colorVectorOfFeatures.Add(Convert.ToSingle(hls.GetAverage(mask).Satuation));
            //colorVectorOfFeatures.Add(Convert.ToSingle(hls.GetAverage(mask).Lightness));
            //colorVectorOfFeatures.Add(Convert.ToSingle(hls.GetSum().Hue));
            //colorVectorOfFeatures.Add(Convert.ToSingle(hls.GetSum().Satuation));
            //colorVectorOfFeatures.Add(Convert.ToSingle(hls.GetSum().Lightness));

            hsv.Dispose();
            lab.Dispose();
            hls.Dispose();
            mask.Dispose();
            gray.Dispose();
            return colorVectorOfFeatures.ToArray();
        }


        public static double[] getHaralickFeatures(Image<Gray, Byte> gray) {
            var glcm = new GrayLevelCooccurrenceMatrix(distance: 1, degree: CooccurrenceDegree.Degree0, normalize: true);
            // Extract the matrix from the image
            double[,] matrix = glcm.Compute(gray.ToBitmap());
            HaralickDescriptor haralick = new HaralickDescriptor(matrix);
            double[] haralickFeatures =  haralick.GetVector();
            return haralickFeatures;                                
        }

        public static double[] colorAtCenter(Image<Bgr, Byte> src) {
           Image<Gray, Byte> gray = src.Convert<Gray, Byte>();
            Image<Gray, Byte> mask = gray.CopyBlank();
            Image<Hsv, float> hsv = src.Convert<Hsv, float>();
            CvInvoke.Threshold(gray, mask, 0, 255, Emgu.CV.CvEnum.ThresholdType.Binary);
            Point center = new Point(src.Width / 2, src.Height / 2);

            MCvScalar hsvColor = new MCvScalar() {
                V0 = hsv.Data[center.Y, center.X, 0],
                V1 = hsv.Data[center.Y,center.X, 1],
                V2 = hsv.Data[center.Y, center.X, 2]
            };
            MCvScalar bgrColor = new MCvScalar() {
                V0 = src.Data[center.Y,center.X, 0],
                V1 = src.Data[center.Y,center.X, 1],
                V2 = src.Data[center.Y,center.X, 2]
            };

            double[] features =new double[]{
                hsvColor.V0,
                hsvColor.V1,
                hsvColor.V2,
                bgrColor.V0,
                bgrColor.V1,
                bgrColor.V2 };
            hsv.Dispose();
            mask.Dispose();
            gray.Dispose();
            return features;            
        }



        public static Matrix<float> vector2matrix(float[] features) {
            Matrix<float> row = new Matrix<float>(new Size(features.Length, 1));
            for(int x = 0; x < features.Length; x++) {
                row[0, x] = features[x];
            }
            return row;
        }
    }
}
