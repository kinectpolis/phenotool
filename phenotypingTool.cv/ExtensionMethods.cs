﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace phenotypingTool.cv {
    public static class ExtensionMethods {
        public static dynamic GetValue(this Mat mat, int row, int col) {
            var value = CreateElement(mat.Depth);
            Marshal.Copy(mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, value, 0, 1);
            return value[0];
        }

        public static void SetValue(this Mat mat, int row, int col, dynamic value) {
            var target = CreateElement(mat.Depth, value);
            Marshal.Copy(target, 0, mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, 1);
        }
        private static dynamic CreateElement(DepthType depthType, dynamic value) {
            var element = CreateElement(depthType);
            element[0] = value;
            return element;
        }

        private static dynamic CreateElement(DepthType depthType) {
            if (depthType == DepthType.Cv8S) {
                return new sbyte[1];
            }
            if (depthType == DepthType.Cv8U) {
                return new byte[1];
            }
            if (depthType == DepthType.Cv16S) {
                return new short[1];
            }
            if (depthType == DepthType.Cv16U) {
                return new ushort[1];
            }
            if (depthType == DepthType.Cv32S) {
                return new int[1];
            }
            if (depthType == DepthType.Cv32F) {
                return new float[1];
            }
            if (depthType == DepthType.Cv64F) {
                return new double[1];
            }
            return new float[1];
        }

        public static float[,] toFloatArray(this DataTable data) {
            var ret= Array.CreateInstance(typeof(float),data.Rows.Count, data.Columns.Count) as float[,];
            for (var i = 0; i < data.Rows.Count - 1; i++)
                for (var j = 0; j < data.Columns.Count - 1; j++)
                    ret[i, j] = Convert.ToSingle(data.Rows[i][j]);
            return ret;
        }

          public static object[,] toObjectArray(this DataTable data) {
            var ret= Array.CreateInstance(typeof(object),data.Rows.Count, data.Columns.Count) as object[,];
            for (var i = 0; i < data.Rows.Count - 1; i++)
                for (var j = 0; j < data.Columns.Count - 1; j++)
                    ret[i, j] = data.Rows[i][j];
            return ret;
        }

        public static int[,] toIntArray(this DataTable data) {
            var ret= Array.CreateInstance(typeof(int),data.Rows.Count, data.Columns.Count) as int[,];
            for (var i = 0; i < data.Rows.Count - 1; i++)
                for (var j = 0; j < data.Columns.Count - 1; j++)
                    ret[i, j] = Convert.ToInt32(data.Rows[i][j]);
            return ret;
        }


    }
}
