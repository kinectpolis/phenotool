﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace phenotypingTool.cv {
    public class cvUtilities {


         /*
        input:
        strength as floating point >= 0.  0 = no change, high numbers equal stronger correction.
        zoom as floating point >= 1.  (1 = no change in zoom)
        */
        public static Task<Image<Bgr, Byte>> correctLentDistorsion(Image<Bgr, Byte> src, float strength, float zoom=1){
            return Task.Run(delegate(){
             double halfWidth = src.Width / 2;
             double halfHeight = src.Height / 2;
             double theta = 0;
             strength = strength == 0 ? Single.Epsilon : strength;
             double correctionRadius = Math.Sqrt(Math.Pow(src.Width,2) + Math.Pow(src.Height,2))  / strength;
            
            Image<Bgr, double> copy= src.Convert<Bgr, double>();
            Image<Bgr, double> blank= copy.CopyBlank();
            
            double[,,] srcData = copy.Data;
            double[,,] dstData = blank.Data;

            int rows = copy.Rows;
            int cols = copy.Cols;
            for (int y = 0; y < rows; ++y) {
                for (int x = 0; x < cols; ++x) {
                    double newX =Convert.ToDouble(x) - halfWidth;
                    double newY =Convert.ToDouble(y)-  halfHeight;
                    double distance = Math.Sqrt(Math.Pow(newX, 2) + Math.Pow(newY, 2));
                    double r = distance / correctionRadius;
                    theta = r == 0 ? 1 : Math.Atan(r) / r;

                    //Console.WriteLine(r);
                    int sourceX = (int) (halfWidth + theta * newX * zoom);
                    int sourceY = (int) (halfHeight + theta * newY * zoom);
                    
                    //Console.WriteLine(String.Format("{0},{1}",sourceX, sourceY));
                    dstData[y, x, 0] = srcData[sourceY, sourceX, 0];
                    dstData[y, x, 1] = srcData[sourceY, sourceX, 1];// / (data[y, x, 0] + data[y, x, 1] + data[y, x, 2]);
                    dstData[y, x, 2] = srcData[sourceY, sourceX, 2];
                }
            }
            copy.Dispose();
            return blank.Convert<Bgr, byte>();
            });
        }

        public static Task<Image<Bgr, Byte>> CorrectLightness(Image<Bgr, Byte> rgb) {
            return Task.Run(delegate(){
                Image<Lab, Byte> lab = rgb.Convert<Lab, Byte>();
                Image<Gray, Byte>[] lab_planes = lab.Split();
                Image<Gray, Byte> lightness = new Image<Gray, byte>(rgb.Size);
                CvInvoke.CLAHE(lab_planes[0], 40, new Size(4, 4), lightness);
                VectorOfMat vm = new VectorOfMat(lightness.Mat, lab_planes[1].Mat, lab_planes[2].Mat);
                CvInvoke.Merge(vm, lab);
                Image<Bgr, Byte> dst = lab.Convert<Bgr, Byte>();
                vm.Dispose();
                lab.Dispose();
                lab_planes = null;
                lightness.Dispose();
                return dst;
            });
        
        }   

        public static Task<Image<Bgr, Byte>> rgb2ChromaticCoordinates(Image<Bgr, Byte> img) {
            return Task.Run(delegate(){
                Image<Bgr, double> copy= img.Convert<Bgr, double>();
                double[,,] data = copy.Data;
                int rows = copy.Rows;
                int cols = copy.Cols;
                for (int y = 0; y < rows; ++y) {
                    for (int x = 0; x < cols; ++x) {
                        double sum = data[y, x, 0] +data[y, x, 1] + data[y, x, 2];
                        data[y, x, 0] = data[y, x, 0] / sum * 255;
                        data[y, x, 1] = data[y, x, 1] / sum * 255;// / (data[y, x, 0] + data[y, x, 1] + data[y, x, 2]);
                        data[y, x, 2] = data[y, x, 2] / sum * 255; // / (data[y, x, 0] + data[y, x, 1] + data[y, x, 2]); 
                    }
                }
                Image<Bgr, Byte> dst = copy.Convert<Bgr, Byte>();
                copy.Dispose();
                return dst;
            });
        }


        public static Task<Image<Bgr, Byte>> normalizedRgb(Image<Bgr, Byte> img) {
             return Task.Run(delegate(){
                    Image<Bgr, double> copy = img.Convert<Bgr, double>();
                    double[,,] data = copy.Data;
                    int rows = copy.Rows;
                    int cols = copy.Cols;
                    for (int y = 0; y < rows; ++y) {
                        for (int x = 0; x < cols; ++x) {
                            double magnitude = Math.Sqrt( Math.Pow(data[y, x, 0],2) + Math.Pow(data[y, x, 1], 2) + Math.Pow(data[y, x, 2], 2));
                            data[y, x, 0] = data[y, x, 0] / magnitude * 255;
                            data[y, x, 1] = data[y, x, 1] / magnitude * 255;// / (data[y, x, 0] + data[y, x, 1] + data[y, x, 2]);
                            data[y, x, 2] = data[y, x, 2] / magnitude * 255; // / (data[y, x, 0] + data[y, x, 1] + data[y, x, 2]); 
                        }
                    }
                    Image<Bgr, Byte> dst = copy.Convert<Bgr, Byte>();
                    copy.Dispose();
                    return dst;
             });
        }

        public static Task<Image<Bgr, Byte>>  whitePatch(Image<Bgr, Byte> img) {
            return Task.Run(delegate(){
                Image<Bgr, float> copy = img.Convert<Bgr, float>();
                float[,,] data = copy.Data;
                int rows = copy.Rows;
                int cols = copy.Cols;
                double[] Min = new double[copy.NumberOfChannels];
                double[] Max = new double[copy.NumberOfChannels];
                System.Drawing.Point[] MinLocations = new System.Drawing.Point[copy.NumberOfChannels];
                System.Drawing.Point[] MaxLocation = new System.Drawing.Point[copy.NumberOfChannels];
                copy.MinMax(out Min, out Max, out MinLocations, out MaxLocation);
                //Console.WriteLine(String.Format("{0},{1},{2}", Max[0], Max[1], Max[2]));
                for (int y = 0; y < rows; ++y) {
                    for (int x = 0; x < cols; ++x) {
                        data[y, x, 0] = (255 / Convert.ToSingle(Max[0])) * data[y, x, 0];
                        data[y, x, 1] = (255 / Convert.ToSingle(Max[1])) * data[y, x, 1];
                        data[y, x, 2] = (255 / Convert.ToSingle(Max[2])) * data[y, x, 2];
                    }
                }
                Image<Bgr, Byte> dst= copy.Convert<Bgr, Byte>();
                copy.Dispose();
                return dst;
            });
        }

        public static Tuple<Matrix<int>,Mat> Kmeans(Mat src, int k) {
            Mat samples = src.Reshape(1, src.Cols * src.Rows);            
            samples.ConvertTo(samples, Emgu.CV.CvEnum.DepthType.Cv32F, 1.0 / 255.0);
            Matrix<int> labels = new Matrix<int>(src.Cols * src.Rows, 1);
            MCvTermCriteria criteria = new  MCvTermCriteria( 100, 1);
            Mat centers = new Mat();
            CvInvoke.Kmeans(samples, k, labels, criteria, 1, Emgu.CV.CvEnum.KMeansInitType.RandomCenters, centers);
            //Console.WriteLine(String.Format("Number of clusters {0}",centers.Rows));            
            return new Tuple<Matrix<int>, Mat>(labels, centers);          
        }

        public static int[] GetContournHierarchy(Mat Hierarchy, int contourIdx){
            #region Get Hierarchy
            int[] ret = new int[] { };

            if(Hierarchy.Depth != Emgu.CV.CvEnum.DepthType.Cv32S) {
                throw new ArgumentOutOfRangeException("ContourData must have Cv32S hierarchy element type.");
            }
            if(Hierarchy.Rows != 1) {
                throw new ArgumentOutOfRangeException("ContourData must have one hierarchy hierarchy row.");
            }
            if(Hierarchy.NumberOfChannels != 4) {
                throw new ArgumentOutOfRangeException("ContourData must have four hierarchy channels.");
            }
            if(Hierarchy.Dims != 2) {
                throw new ArgumentOutOfRangeException("ContourData must have two dimensional hierarchy.");
            }
            long elementStride = Hierarchy.ElementSize / sizeof(Int32);
            var offset0 = (long)0 + contourIdx * elementStride;
            if(0 <= offset0 && offset0 < Hierarchy.Total.ToInt64() * elementStride) {

                var offset1 = (long)1 + contourIdx * elementStride;
                var offset2 = (long)2 + contourIdx * elementStride;
                var offset3 = (long)3 + contourIdx * elementStride;
                ret = new int[4];
                unsafe {
                    //return *((Int32*)Hierarchy.DataPointer.ToPointer() + offset);

                    ret[0] = *((Int32*)Hierarchy.DataPointer.ToPointer() + offset0);
                    ret[1] = *((Int32*)Hierarchy.DataPointer.ToPointer() + offset1);
                    ret[2] = *((Int32*)Hierarchy.DataPointer.ToPointer() + offset2);
                    ret[3] = *((Int32*)Hierarchy.DataPointer.ToPointer() + offset3);
                }
            }
            return ret; 
            #endregion
        }
        //private static List<Mat> showClusters(ref Mat src, Matrix<int> labels, Mat centers) {
        //    centers.ConvertTo(centers, Emgu.CV.CvEnum.DepthType.Cv8U, 255.0);
        //    centers.Reshape(src.NumberOfChannels);
        //    List<Mat> clusters = new List<Mat>();
        //    for (int i = 0; i < centers.Rows; i++) {
        //        //clusters.add(Mat.zeros(cutout.size(), cutout.type()));
        //        clusters.Add(centers.Clone());
        //    }
        //    int rows = src.Rows;
        //    int cols = src.Cols;
        //    int index = 0;
        //    for (int y = 0; y < rows; ++y) {
        //        for (int x = 0; x < cols; ++x) {
        //            int label = labels[index,0];
        //            //Console.WriteLine(String.Format("{0}", label));
        //            index++;
        //        }
        //    }
        //    return clusters;
        //}

        /*private static List<Mat> showClusters(Mat cutout, Mat labels, Mat centers) {
            centers.convertTo(centers, CvType.CV_8UC1, 255.0);
            centers.reshape(3); //rgb

            List<Mat> clusters = new ArrayList<Mat>();
            for (int i = 0; i < centers.rows(); i++) {
                //clusters.add(Mat.zeros(cutout.size(), cutout.type()));
                clusters.add(Mat.zeros(cutout.size(), CvType.CV_8U));
            }

            Map<Integer, Integer> counts = new HashMap<Integer, Integer>();
            for (int i = 0; i < centers.rows(); i++) counts.put(i, 0);

            int rows = 0;
            for (int y = 0; y < cutout.rows(); y++) {
                for (int x = 0; x < cutout.cols(); x++) {
                    int label = (int)labels.get(rows, 0)[0];

                    int rout = (int)cutout.get(y, x)[2]; //(int)centers.get(label, 2)[0];
                    int gout = (int)cutout.get(y, x)[1]; //(int)centers.get(label, 1)[0];
                    int bout = (int)cutout.get(y, x)[0]; //(int)centers.get(label, 0)[0];
                    if (rout != 0 || gout != 0 || bout != 0) {
                        int r = (int)centers.get(label, 2)[0];
                        int g = (int)centers.get(label, 1)[0];
                        int b = (int)centers.get(label, 0)[0];

                        counts.put(label, counts.get(label) + 1);
                        ///clusters.get(label).put(y, x, b, g, r);
                        clusters.get(label).put(y, x, 255);

                    }
                    //clusters.get(label).put(y, x, 255, 255, 255);
                    rows++;
                }
            }
            //System.out.println(counts);
            return clusters;
        }*/
    }
}
