﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace phenotypingTool {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }
  

        private void countToolStripMenuItem_Click(object sender, EventArgs e) {
            phenotypingTool.beans.pollen.MainForm win = new beans.pollen.MainForm();
            win.WindowState = FormWindowState.Maximized;
            win.StartPosition = FormStartPosition.CenterScreen;
            win.MdiParent = this;
            win.Show();
        }

        private void countToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            phenotypingTool.box.beans.count.MainForm win = new  box.beans.count.MainForm();
            win.WindowState = FormWindowState.Maximized;
            win.StartPosition = FormStartPosition.CenterScreen;
            win.MdiParent = this;
            win.Show();
        }

        private void spotlightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            phenotypingTool.box.beans.spotlight.MainForm win = new  box.beans.spotlight.MainForm();
            win.WindowState = FormWindowState.Maximized;
            win.StartPosition = FormStartPosition.CenterScreen;
            win.MdiParent = this;
            win.Show();
        }

        
    }
}
