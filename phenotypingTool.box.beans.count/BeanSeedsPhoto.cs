﻿
using Accord.Imaging;
using Accord.Imaging.Filters;
using Accord.Math.Geometry;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using phenotypingTool.common;
using phenotypingTool.cv;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace phenotypingTool.box.beans.count
{

    public struct Bean
    {
        public Bitmap img;
        public double area;
    }

    class BeanSeedsPhoto : Photo{
        public Image<Bgr, Byte> ir {get; set; }
        public String pathGray {set; get; }
        public String pathImages;
      
        public List<Bean> seeds;      

        public BeanSeedsPhoto(String colorPath, String grayPath, int w, int h){
            try{
            this.seeds = new List<Bean>();
            this.images = new Dictionary<string, System.Drawing.Bitmap>();
            this.path = colorPath; 
            this.pathGray = grayPath;
            this.color = new Image<Bgr, byte>(this.path);
            this.ir = new Image<Bgr, byte>(this.pathGray);
            this.color = this.color.Resize(w, h, Emgu.CV.CvEnum.Inter.Linear, true);
            this.ir = this.ir.Resize(w, h, Emgu.CV.CvEnum.Inter.Linear, true);
            this.pathImages = Path.Combine(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "seedsGray");
            if(!Directory.Exists(this.pathImages))
                Directory.CreateDirectory(this.pathImages);
            
            }
            catch(Exception ex){
                throw new Exception(String.Format("Error reading the file {0} : {1}", Path.GetFileNameWithoutExtension(colorPath), ex));
            }
        }

          public override Task extractBackground(){
            return Task.Run(async delegate (){
                 this.color = await cvUtilities.correctLentDistorsion(this.color,1.25f);
                 this.ir = await cvUtilities.correctLentDistorsion(this.ir,1.25f);
                 Size sz = this.color.Size;
                 Point loc = new Point(80, 40);
                 Rectangle roi = new Rectangle(100,60, sz.Width - loc.X - 110, sz.Height - loc.Y - 110); 
                 this.color = this.color.Copy(roi);
                 this.ir = this.ir.Copy(roi);
                 Image<Gray, Byte> gray = this.ir.Convert<Gray, Byte>();
                 CvInvoke.CLAHE(gray, 1.0, new Size(2, 2), gray);
                 this.mask = gray.CopyBlank();
                 CvInvoke.Threshold(gray, this.mask, 0, 255, Emgu.CV.CvEnum.ThresholdType.Otsu | Emgu.CV.CvEnum.ThresholdType.Binary);
                 Mat kernel = CvInvoke.GetStructuringElement(Emgu.CV.CvEnum.ElementShape.Ellipse, new Size(3, 3), new Point(-1, -1));
                 mask._MorphologyEx(Emgu.CV.CvEnum.MorphOp.Open, kernel, new Point(-1, -1), 10, Emgu.CV.CvEnum.BorderType.Default, new MCvScalar(1.0));
                 gray.Dispose();
            });
        }

        public override Task extractContourns()
        {
              return Task.Run(delegate(){
                
                //watershed
                UnmanagedImage input = UnmanagedImage.FromManagedImage(mask.ToBitmap());
                var dt = new BinaryWatershed(0.7f, DistanceTransformMethod.Euclidean);
                Bitmap output = dt.Apply(input.ToManagedImage());
                BlobCounter bc = new BlobCounter( );                
                bc.ProcessImage( output );
                //SimpleShapeChecker shapeChecker = new SimpleShapeChecker();                
                //Rectangle[] rects = bc.GetObjectsRectangles( );                
                //get objects information 
                Blob[] blobs = bc.GetObjectsInformation();      
                //marking image
                this.color.Draw(blobs.Count().ToString(),new Point(40,40), Emgu.CV.CvEnum.FontFace.HersheyPlain, 1,new Bgr(255,255,255),2 );                
                UnmanagedImage result = UnmanagedImage.FromManagedImage(this.color.ToBitmap());
                Bitmap original = this.color.ToBitmap();
                var marker = new PointsMarker(dt.MaxPoints,Color.Magenta, 5);
                marker.ApplyInPlace(result);
                        foreach(Blob blob in blobs){
                      
                            List<Accord.IntPoint> border= bc.GetBlobsEdgePoints(blob);
                            //blob.Image.ToManagedImage().Save(String.Format("{0}.jpg", Guid.NewGuid().ToString()));                    
                            bc.ExtractBlobsImage(original, blob, false);
                            //this.images.Add(Guid.NewGuid().ToString(), blob.Image.ToManagedImage());//create marked image
                            //blob.Image.ToManagedImage().Save(string.Format("{0}/{1}.jpg", this.pathImages, Guid.NewGuid().ToString()));
                            //CvInvoke.FindNonZero(blob.Image,points);
                            //Image<Bgr,Byte> seedColor = new Image<Bgr, byte>(blob.Image.ToManagedImage());
                            //Image<Gray,Byte> seedGray = seedColor.Convert<Gray, Byte>();
                            //seedGray.Save(string.Format("{0}/{1}.jpg", this.pathImages, Guid.NewGuid().ToString()));
                            //VectorOfPoint contour = new VectorOfPoint();
                            //CvInvoke.FindNonZero(seedGray,contour);
                            //double area =CvInvoke.ContourArea(contour);
                    
                            //double perimeter = CvInvoke.ArcLength(new VectorOfPoint(border.Select( pt=> new Point(pt.X, pt.Y)).ToArray()), true);
                            //double shape = (4 * Math.PI * blob.Area) / Math.Pow(perimeter, 2); 
                            //Console.WriteLine(shape);
                            ////CvInvoke.FillConvexPoly(this.color,contour,new MCvScalar(0,255,0), Emgu.CV.CvEnum.LineType.FourConnected);
                            this.seeds.Add(new Bean(){  img = blob.Image.ToManagedImage(), area = blob.Area });            
                            marker = new PointsMarker(border, Color.Magenta, 3);
                            marker.ApplyInPlace(result);
                            //CvInvoke.PutText(this.color,String.Format("{0:0.##}", shape),blob.Rectangle.Location, Emgu.CV.CvEnum.FontFace.HersheyPlain, 1,new MCvScalar(255,255,255),2);
                         }
                        //CvInvoke.PutText(this.color,blobs.Length.ToString(),new Point(40,40), Emgu.CV.CvEnum.FontFace.HersheyPlain, 2,new MCvScalar(255,255,255),2);
                        this.images.Add("watershed", result.ToManagedImage());//create marked image
                       
                //BlobCounter bc = new BlobCounter(marked);
                //// Extract blobs
                //Blob[] blobs = bc.GetObjectsInformation();
                //bc.ExtractBlobsImage(marked, blobs[0], true);
                //// Extract blob's edge points
                //List<Accord.IntPoint> contour2= bc.GetBlobsEdgePoints(blobs[0]); 
                //// Create a green, 2 pixel width points marker's instance
                // marker = new PointsMarker(contour2, Color.Green, 2);      
                //// Apply the filter in a given color image
                //marker.ApplyInPlace(input);

               
                
            });
        }

        public async override Task process()
        {
            await this.extractBackground();
            await this.extractContourns();
            
            this.images.Add("color", this.color.ToBitmap());//add the color image to the dic object
            //this.images.Add("gray", this.ir.ToBitmap());//add the gray image to the dic object
            //this.images.Add("mask", this.mask.ToBitmap());//add the gray image to the dic object
            this.color.Dispose();
            this.ir.Dispose();
            this.mask.Dispose();
        }
    }
}
