﻿using Emgu.CV;
using Emgu.CV.Structure;
using phenotypingTool.common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace phenotypingTool.box.beans.count
{
    public partial class MainForm : Form
    {
        private readonly SynchronizationContext synchronizationContext;
        private List<BeanSeedsPhoto> photos;

        public MainForm()
        {
            InitializeComponent();
            this.photos = new List<BeanSeedsPhoto>();
            this.synchronizationContext = SynchronizationContext.Current;

        }

        private async void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    var files = Directory.EnumerateFiles(dialog.SelectedPath);
                    txtSelectedPath.Text = dialog.SelectedPath;
                    files = files.Where(f => Utilities.ImageExtensions.Contains(Path.GetExtension(f.ToUpper())));
                    if (files.Count() > 0){
                        this.flpImages.Controls.Clear();
                        await this.readImages(files.ToArray());
                        //MessageBox.Show("Images Loaded", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Images noWt found", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Task readImages(string[] files)
        {
            #region read images
            this.photos = new List<BeanSeedsPhoto>();
            string[] filesFiltered = files.Select(path =>
            {
                string fileName = Path.GetFileNameWithoutExtension(path);
                int prefixIdx = fileName.LastIndexOf("_");
                return prefixIdx > 0 ? fileName.Substring(0, fileName.LastIndexOf("_")) : null;
            })
            .Distinct()
            .ToArray();


            return Task.WhenAll(
            filesFiltered.Select(fileName =>
                Task.Run(async delegate ()
                {
                    //Console.WriteLine(fileName);
                    if (fileName != null)
                    {
                        String[] images = files.Where(fname => Path.GetFileNameWithoutExtension(fname).StartsWith(fileName)).ToArray();
                        if (images.Length == 2)
                        {
                            String colorPath = images.Where(name => name.Contains("color")).First();
                            String grayPath = images.Where(name => name.Contains("gray")).First();
                            if (colorPath != null && grayPath != null)
                            {
                                var image = new BeanSeedsPhoto(colorPath, grayPath, 1024, 780);
                                await image.process();
                                this.photos.Add(image);
                                synchronizationContext.Send(new SendOrPostCallback(o =>
                                {
                                    this.showImage(o as BeanSeedsPhoto);
                                }), image);
                            }
                        }
                    }
                }))); 
            #endregion
        }

        private PictureBox createPictureBox(Bitmap img, Object tag = null)
        {
            #region make Picture Box
            PictureBox pbx = new PictureBox();
            pbx.Image = img;
            pbx.Size = new Size(150, 150);
            pbx.BorderStyle = BorderStyle.FixedSingle;
            pbx.Margin = new Padding(10);
            pbx.Cursor = Cursors.Hand;
            pbx.Tag = tag;
            pbx.SizeMode = PictureBoxSizeMode.Zoom;
            pbx.Dock = DockStyle.Fill;
            pbx.DoubleClick += displayImage_PictureBox_Click;
            //img.Dispose();
            return pbx;
            #endregion
        }                                 

        private void displayImage_PictureBox_Click(object sender, EventArgs e)
        {

            PictureBox pbox = sender as PictureBox;
            Image<Bgr, Byte> imageClicked = new Image<Bgr, Byte>(pbox.Image as Bitmap);
            Emgu.CV.UI.ImageViewer viewer = new Emgu.CV.UI.ImageViewer(imageClicked);
            viewer.ShowDialog();
        }

        private void showImage(Photo pic)
        {
            #region createContainer
            Panel pnlLayout = new Panel();
            pnlLayout.Size = new Size(180, 220);
            pnlLayout.BorderStyle = BorderStyle.FixedSingle;

            //tab control
            TabControl tabControl = new TabControl();
            tabControl.Size = new Size(180, 180);
            tabControl.Dock = DockStyle.Top;
            //tabControl.Font = new Font("Arial", 7);
            tabControl.Appearance = TabAppearance.Normal;
            tabControl.Cursor = Cursors.Hand;

            foreach (var entry in pic.images)
            {
                //color tab page
                PictureBox pictureBox = this.createPictureBox(entry.Value);
                TabPage tabPage = new TabPage();
                tabPage.Text = entry.Key;
                tabPage.Controls.Add(pictureBox);
                tabControl.TabPages.Add(tabPage);
            }

            String id = Path.GetFileName(pic.path);
            Panel pnlInfo = new Panel();
            pnlInfo.Size = new Size(180, 60);
            pnlInfo.Dock = DockStyle.Bottom;
            Label lblFileName = new Label();
            //lblFileName.BorderStyle = BorderStyle.FixedSingle;
            lblFileName.Text = id;
            lblFileName.Dock = DockStyle.Fill;
            pnlInfo.Controls.Add(lblFileName);
            pnlLayout.Controls.Add(pnlInfo);
            pnlLayout.Controls.Add(tabControl);
            this.flpImages.Controls.Add(pnlLayout);
            #endregion
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try{
                String pathCsv = Path.Combine(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath), "data.csv");
                if(File.Exists(pathCsv))
                     File.Delete(pathCsv); 
                    using (var csv = new StreamWriter(pathCsv, true)){
                        foreach (BeanSeedsPhoto photo in this.photos){
                            double avgArea = photo.seeds.Select(bean => bean.area).Sum() / photo.seeds.Count() ;
                            csv.WriteLine(String.Format("{0},{1}", Path.GetFileNameWithoutExtension(photo.path), String.Format("{0:0.##}", avgArea)));
                            csv.Flush();
                        }   
                    }
                MessageBox.Show("Done!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
