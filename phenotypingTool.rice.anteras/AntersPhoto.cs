﻿using System.Threading.Tasks;
using phenotypingTool.common;
using Emgu.CV;
using Emgu.CV.Structure;
using phenotypingTool.cv;
using System.Drawing;
using System.Collections.Generic;
using System;

namespace phenotypingTool.rice.anteras {
    public class AntersPhoto : Photo{
        public AntersPhoto(string path) {
            this.path = path;
            this.color = new Image<Bgr, byte>(path);
            this.mask = new Image<Gray, byte>(this.color.Size);
            this.images = new Dictionary<string,Bitmap>();
        }

        public override Task extractBackground() {
            return Task.Run(async delegate(){
                 this.color = await cvUtilities.CorrectLightness(this.color);
                 Image<Gray, byte> gray = this.color.Convert<Gray, byte>();
                 //Image<Gray, Byte> laplace = gray.Laplace(31).Convert<Gray, Byte>();
                 this.images.Add("color",this.color.ToBitmap());
                 Image<Hsv, byte> model = this.color.Convert<Hsv, Byte>();
                 Image<Gray, byte>[] channels = model.Split();
                 for(int i = 0; i < channels.Length; i++)
                    this.images.Add(String.Format("{0}", i), channels[i].ToBitmap());
                 gray.Dispose();
                 model.Dispose();  
                 channels = null;
            });
        }

        public override Task extractContourns() {
            throw new System.NotImplementedException();
        }

        public async override Task process() {
            await this.extractBackground();
        }
    }
}