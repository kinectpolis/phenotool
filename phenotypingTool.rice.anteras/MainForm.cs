﻿using Emgu.CV;
using Emgu.CV.Structure;
using phenotypingTool.common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace phenotypingTool.rice.anteras {
    public partial class MainForm : Form {
        
        private readonly SynchronizationContext synchronizationContext;
        
        public MainForm() {
            InitializeComponent();
            this.synchronizationContext = SynchronizationContext.Current;
        }

        private async void btnBrowseFolder_Click(object sender, EventArgs e) {
            try {
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                if (dialog.ShowDialog() == DialogResult.OK) {
                    var files = Directory.EnumerateFiles(dialog.SelectedPath);
                    txtPathImages.Text = dialog.SelectedPath;
                    files = files.Where(f => Utilities.ImageExtensions.Contains(Path.GetExtension(f.ToUpper())));
                    if (files.Count() > 0) {                         
                        await this.readImages(files.ToArray());
                        //MessageBox.Show("Images Loaded", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else {
                        MessageBox.Show("Images noWt found", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
             }
            catch (Exception ex) {
                MessageBox.Show(ex.Message,"", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Task readImages(string[] files) {
            return Task.WhenAll(
               files.Select(imagePath =>
                   Task.Run(async delegate (){
                        AntersPhoto photo = new AntersPhoto(imagePath);
                        await photo.process();
                       synchronizationContext.Send(new SendOrPostCallback(o =>{
                           this.showImage(o as AntersPhoto);
                       }), photo);
                   }
               )));  
        }

        private void showImage(AntersPhoto image) {
            Panel pnlLayout = new Panel();
            pnlLayout.Size = new Size(180, 220);
            pnlLayout.BorderStyle = BorderStyle.FixedSingle;

            //tab control
            TabControl tabControl = new TabControl();
            tabControl.Size = new Size(180, 180);
            tabControl.Dock = DockStyle.Top;
            //tabControl.Font = new Font("Arial", 7);
            tabControl.Appearance = TabAppearance.Normal;
            tabControl.Cursor = Cursors.Hand;

            PictureBox pictureBox = createPictureBox(image.color.ToBitmap());
            TabPage tabPage = new TabPage();
            tabPage.Text = "color";   
            tabPage.Controls.Add(pictureBox);
            tabControl.TabPages.Add(tabPage);

            foreach (var entry in image.images){                
                pictureBox = this.createPictureBox(entry.Value);
                tabPage = new TabPage();
                tabPage.Text = entry.Key;   
                tabPage.Controls.Add(pictureBox);
                tabControl.TabPages.Add(tabPage);
            }
            
            Panel pnlInfo = new Panel();
            pnlInfo.Size = new Size(180, 60);
            pnlInfo.Dock = DockStyle.Bottom;
            Label lblFileName = new Label();
            //lblFileName.BorderStyle = BorderStyle.FixedSingle;
            //lblFileName.Text = Path.GetFileName(pic.path);
            lblFileName.Dock = DockStyle.Fill;
            pnlInfo.Controls.Add(lblFileName);
            pnlLayout.Controls.Add(pnlInfo);
            pnlLayout.Controls.Add(tabControl);
            this.flImages.Controls.Add(pnlLayout);
            
        }

        private PictureBox createPictureBox(Bitmap img, Object tag = null)
        {
            #region make Picture Box
            PictureBox pbx = new PictureBox();
            pbx.Image = img;
            pbx.Size = new Size(150, 150);
            pbx.BorderStyle = BorderStyle.FixedSingle;
            pbx.Margin = new Padding(10);
            pbx.Cursor = Cursors.Hand;
            pbx.Tag = tag;
            pbx.SizeMode = PictureBoxSizeMode.Zoom;
            pbx.Dock = DockStyle.Fill;
            pbx.DoubleClick += displayImage_PictureBox_Click;
            //img.Dispose();
            return pbx; 
            #endregion
        }

        private void displayImage_PictureBox_Click(object sender, EventArgs e) {
             PictureBox pbox = sender as PictureBox;
            Image<Bgr, Byte> imageClicked = new Image<Bgr, Byte>(pbox.Image as Bitmap);
            Emgu.CV.UI.ImageViewer viewer = new Emgu.CV.UI.ImageViewer(imageClicked);
            viewer.ShowDialog();
          
        }
    }
}
