﻿using Accord.Imaging.Filters;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using phenotypingTool.common;
using phenotypingTool.cv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace phenotypingTool.hb {
    public partial class MainForm : Form {

        private readonly SynchronizationContext synchronizationContext;
        private Emgu.CV.OCR.Tesseract ocr;
        private StringBuilder csv;

        public MainForm() {
            ocr = new Emgu.CV.OCR.Tesseract("", "eng", Emgu.CV.OCR.OcrEngineMode.Default);
            csv = new StringBuilder();
            InitializeComponent();
            this.synchronizationContext = SynchronizationContext.Current;
        }


        #region makePictureBox
        private PictureBox makePictureBox(Bitmap img, Object tag = null) {
            PictureBox pbx = new PictureBox();
            pbx.Image = img;
            pbx.Size = new Size(150, 150);
            pbx.BorderStyle = BorderStyle.FixedSingle;
            pbx.Margin = new Padding(10);
            pbx.Cursor = Cursors.Hand;
            pbx.Tag = tag;
            pbx.SizeMode = PictureBoxSizeMode.Zoom;
            pbx.Dock = DockStyle.Fill;
            pbx.Click += displayImage_PictureBox_Click;
            //img.Dispose();
            return pbx;
        }

        #endregion

        private void displayImage_PictureBox_Click(object sender, EventArgs e) {
            PictureBox pbox = sender as PictureBox;
            Image<Bgr, Byte> imageClicked = new Image<Bgr, Byte>(pbox.Image as Bitmap);
            //CvInvoke.NamedWindow("image", Emgu.CV.CvEnum.NamedWindowType.KeepRatio);
            //CvInvoke.Imshow("image", imageClicked);
            Emgu.CV.UI.ImageViewer viewer = new Emgu.CV.UI.ImageViewer(imageClicked);
            viewer.ShowDialog();
          
        }

        private async void btnBrowseFolder_ClickAsync(object sender, EventArgs e) {
            try {
                FolderBrowserDialog dialog = new FolderBrowserDialog();
                if (dialog.ShowDialog() == DialogResult.OK) {
                    var files = Directory.EnumerateFiles(dialog.SelectedPath);
                    txtSelectedPath.Text = dialog.SelectedPath;
                    files = files.Where(f => Utilities.ImageExtensions.Contains(Path.GetExtension(f.ToUpper())));
                        if (files.Count() > 0) {
                            this.flpImages.Controls.Clear();
                            csv.Clear();
                            await this.loadImagesIntoGUI(files.ToArray());
                            File.WriteAllText("data.csv", csv.ToString());
                            MessageBox.Show("Images Loaded","", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else {
                            MessageBox.Show("Images not found","", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                }
            }
            catch (Exception ex) {
                MessageBox.Show("", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Task loadImagesIntoGUI(string[] imageFiles) {
                                               
            return Task.WhenAll(
                imageFiles.Select(imagePath =>
                    Task.Run(delegate () {
                        int padding = 80;
                        Dictionary<String, Bitmap> images = new Dictionary<string, Bitmap>();
                        Image<Bgr, Byte> raw = new Image<Bgr, byte>(imagePath);
                        raw = raw.Resize(1024, 780, Emgu.CV.CvEnum.Inter.Linear, true);
                        Rectangle roi = new Rectangle(0, padding, raw.Width, raw.Height - padding);
                        Image<Bgr, Byte> rgb = raw.Copy(roi);
                        //raw = rgb.Clone();
                        
                        //ImageProcessingUtilities.correctLightness(ref rgb);
                        //ImageProcessingUtilities.normalizedRgb(ref rgb);
                        //cvUtilities.rgb2ChromaticCoordinates(ref rgb);
                       // cvUtilities.whitePatch(ref rgb);
                        Image<Gray, Byte>[] channels = rgb.Split();
                        Image<Gray, float> mask1 = new Image<Gray, float>(rgb.Size);

                        //egi = mat2gray(3 * NormalizedGreen - 1);
                        
                        Image<Gray, float> NormalizedGreen = channels[1].Convert<Gray, float>();
                        Image<Gray, float> NormalizedRed = channels[2].Convert<Gray, float>();
                        Image<Gray, float> NormalizedBlue = channels[0].Convert<Gray, float>();
                        CvInvoke.Normalize(NormalizedGreen, NormalizedGreen, 0, 1, Emgu.CV.CvEnum.NormType.MinMax);
                        CvInvoke.Normalize(NormalizedRed, NormalizedRed, 0, 1, Emgu.CV.CvEnum.NormType.MinMax);
                        CvInvoke.Normalize(NormalizedBlue, NormalizedBlue, 0, 1, Emgu.CV.CvEnum.NormType.MinMax);

                        //Image<Gray, float> vindex = new Image<Gray, float>(rgb.Size);
                        
                        //Image<Gray, float> vindex = (3 * NormalizedGreen - 1);
                        Image<Gray, float> vindex = NormalizedGreen - NormalizedRed ;//(3 * NormalizedGreen - 1);
                        //CvInvoke.Divide((NormalizedGreen - NormalizedRed),(NormalizedGreen + NormalizedRed),vindex);//(3 * NormalizedGreen - 1);
                        //Image<Gray, Byte> egi2gray = egi.Convert<Gray, Byte>();
                        //vindex = (Math.Sqrt(3) * (NormalizedGreen - NormalizedBlue));
                        CvInvoke.Threshold(vindex,mask1, 0.4, 1, Emgu.CV.CvEnum.ThresholdType.Binary);
                        mask1 = mask1 * 255;
                        vindex = vindex * 255;

                        

                        /*double[] Min = new double[vindex.NumberOfChannels];
                        double[] Max = new double[vindex.NumberOfChannels];
                        Point[] MinLocations = new Point[vindex.NumberOfChannels];
                        Point[] MaxLocation = new Point[vindex.NumberOfChannels];
                        mask.MinMax(out Min, out Max, out MinLocations, out MaxLocation);
                        Console.WriteLine(String.Format("{0}-{1}", Min[0], Max[0]));*/

                        //Image<Gray, Byte> gray = channels[1].Copy(roi);
                        //CvInvoke.Threshold(channels[2], channels[2], 0, 255, Emgu.CV.CvEnum.ThresholdType.Otsu | Emgu.CV.CvEnum.ThresholdType.ToZeroInv);
                        //using (VectorOfMat vm = new VectorOfMat(channels.Select(m => m.Mat).ToArray())) {
                        //    CvInvoke.Merge(vm, hsv);
                        //}

                        Image<Gray, byte> mask0 = new Image<Gray, byte>(rgb.Size);
                        CvInvoke.Threshold(channels[0], mask0, 200, 255, Emgu.CV.CvEnum.ThresholdType.BinaryInv);
                        Image<Bgr, Byte> src0 = raw.Copy(roi).Copy(mask0);

                        float whitePixelsTotalArea = CvInvoke.CountNonZero(mask0);
                        float whitePixelshealthyArea = CvInvoke.CountNonZero(mask1);
                        float whitePixelsAfectedArea = whitePixelsTotalArea - whitePixelshealthyArea;
                        float pAreaFolearHealthy = (whitePixelshealthyArea * 100) / whitePixelsTotalArea;
                        float pAreaFolearAffected = 100 - pAreaFolearHealthy;

                        
                        /*
                        whitePixelsAllRegion = nnz(BW);
                        whitePixelsHealthyRegion = nnz(bwHealthy);
                        whitePixelsAffectedRegion = whitePixelsAllRegion - whitePixelsHealthyRegion;
                        pAreaFolearHealthy = (whitePixelsHealthyRegion * 100) / whitePixelsAllRegion
                        pAreaFolearAffected = 100 - pAreaFolearHealthy

                         */
                        images.Add("raw", raw.ToBitmap());
                        images.Add("roi", src0.ToBitmap());
                        images.Add("rgb", rgb.ToBitmap());                        
                        images.Add("mask",mask1.ToBitmap());

                        synchronizationContext.Send(new SendOrPostCallback(o => {
                            Object[] args = o as Object[];
                            var newLine = string.Format("{0},{1}", args[1] as string, pAreaFolearHealthy.ToString("0.00"));
                            csv.AppendLine(newLine);
                            Control imageContainer = this.createContainer(args[0] as Dictionary<String, Bitmap>, args[1] as string, pAreaFolearHealthy.ToString("0.00"), pAreaFolearAffected.ToString("0.00"));
                            this.flpImages.Controls.Add(imageContainer);
                        }), new Object[] { images, Path.GetFileName(imagePath) });

                        rgb.Dispose();
                        mask0.Dispose();
                        mask1.Dispose();
                        vindex.Dispose();
                        NormalizedBlue.Dispose();
                        NormalizedGreen.Dispose();
                        NormalizedRed.Dispose();
                        
                        /*raw.Dispose();
                        hsv.Dispose();*/
                        channels = null;
                    })
            )
        );
        }

        private Control createContainer(Dictionary<string, Bitmap> dictionary,params string[] args) {
            Panel pnlLayout = new Panel();
            pnlLayout.Size = new Size(180, 220);
            pnlLayout.BorderStyle = BorderStyle.FixedSingle;

            //tab control
            TabControl tabControl = new TabControl();
            tabControl.Size = new Size(180, 180);
            tabControl.Dock = DockStyle.Top;
            //tabControl.Font = new Font("Arial", 7);
            tabControl.Appearance = TabAppearance.Normal;
            tabControl.Cursor = Cursors.Hand;            

            foreach (var entry in dictionary) {
                //color tab page
                PictureBox inImagePictureBox = this.makePictureBox(entry.Value);
                TabPage inImagePage = new TabPage();
                inImagePage.Text = entry.Key;

                inImagePage.Controls.Add(inImagePictureBox);
                tabControl.TabPages.Add(inImagePage);
                
            }


            TableLayoutPanel tblLayoutInfo = new TableLayoutPanel();
            tblLayoutInfo.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tblLayoutInfo.RowCount = 3;
            tblLayoutInfo.Size = new Size(180, 60);
            tblLayoutInfo.Dock = DockStyle.Bottom;
            tblLayoutInfo.AutoSize = true;
            tblLayoutInfo.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 50));

            tblLayoutInfo.Controls.Add(new Label() { Text = "File:", Font = new Font(new FontFamily("Arial"), 9, FontStyle.Bold) }, 0, 0);
            
            Label lblFileName = new Label();
            lblFileName.Text = args[0];
            //lblFileName.BorderStyle = BorderStyle.FixedSingle;
            tblLayoutInfo.Controls.Add(lblFileName,1,0);

            tblLayoutInfo.Controls.Add(new Label() { Text = "Area:", Font = new Font(new FontFamily("Arial"),9,FontStyle.Bold) }, 0, 1);
            Label lblInfo = new Label();
            lblInfo.Text = String.Format("{0}+/{1}-", args[1], args[2]);
            //lblInfo.BorderStyle = BorderStyle.FixedSingle;
            tblLayoutInfo.Controls.Add(lblInfo,1,1);

           
            pnlLayout.Controls.Add(tblLayoutInfo);
            pnlLayout.Controls.Add(tabControl);
            
            return pnlLayout;
        }
    }
}
