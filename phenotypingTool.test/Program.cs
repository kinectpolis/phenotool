﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using Emgu.CV.Util;
using MathNet.Numerics.Statistics;
using phenotypingTool.cv;
using System.IO;
using System.Runtime.InteropServices;


namespace phenotypingTool.test
{
    class Program {      

        static  void Main(string[] args) {

           String path = @"C:\train\samples";
            string pathCsv = Path.Combine(Directory.GetParent(path).FullName, "dataset_.csv");
                if(File.Exists(pathCsv)) {
                    File.Delete(pathCsv);
                }

            foreach(var dir in Directory.EnumerateDirectories(path)) {
               Console.WriteLine(String.Format("*********{0}",Path.GetFileName(dir)));                
                #region get label
                string label = Path.GetFileName(dir);
                if(label.Equals("viable"))
                    label = "1";
                else if(label.Equals("nonViable"))
                    label = "2";
                else
                    label = "3"; 
                #endregion
                foreach(var file in Directory.GetFiles(dir)) {
                    if(File.Exists(file))
                        using(Image<Bgr, byte> src = new Image<Bgr, byte>(file)) {
                            Image<Hsv,Byte> hsv = src.Convert<Hsv, Byte>();
                            Image<Gray,Byte> gray = src.Convert<Gray, Byte>();
                            Image<Gray,Byte> mask = gray.CopyBlank();
                            CvInvoke.Threshold(gray, mask, 0, 255,Emgu.CV.CvEnum.ThresholdType.Binary);

                            String vector_of_features_as_string = String.Join(",",new String[]{
                                    String.Join(",", FeaturesUtilities.image_characteristics_to_feature(src)),
                                    String.Join(",",FeaturesUtilities.colorAtCenter(src)),
                                    String.Join(",",FeaturesUtilities.getHaralickFeatures(gray))

                                 });
                                using(var csv = new StreamWriter(pathCsv, true)) { 
                                    csv.WriteLine(String.Format("{0},{1},{2}",
                                            Path.GetFileNameWithoutExtension(file),
                                            vector_of_features_as_string,                                             
                                            label
                                        ));
                                
                                    csv.Flush();
                                }
                            hsv.Dispose();
                   
                        }
                }
            }

           
          
        }
     
    }
}
